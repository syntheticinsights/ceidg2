using NUnit.Framework;
using System;

using CEIDGLib;

namespace CEIDGTest
{
    [TestFixture()]
    public class DataVerifierTestClass
    {
        [Test()]
        public void TestVerifyCompanyName()
        {
            Assert.DoesNotThrow(delegate {DataVerifier.VerifyCompanyName("<|poprawna nazwa%**&");});

            Assert.Throws(typeof(DataVerifier.EmptyFieldException), delegate {
                DataVerifier.VerifyCompanyName("");
            });
        }

        [Test()]
        public void TestVerifyNIP()
        {
            Assert.DoesNotThrow(delegate { // some NIP examples from real CEIDG
                DataVerifier.VerifyNIP("7511702899");
                DataVerifier.VerifyNIP("7511308837");
                DataVerifier.VerifyNIP("7511316162");
                DataVerifier.VerifyNIP("7511057588");
                DataVerifier.VerifyNIP("7511711243");
                DataVerifier.VerifyNIP("7511148960");
            });

            Assert.Throws(typeof(DataVerifier.EmptyFieldException), delegate {
                DataVerifier.VerifyNIP("");
            });

            Assert.Throws(typeof(DataVerifier.IllegalCharactersException), delegate {
                DataVerifier.VerifyNIP("75110k7588");
            });

            Assert.Throws(typeof(DataVerifier.InvalidNumberException), delegate {
                DataVerifier.VerifyNIP("75117112431"); // bad length
            });

            Assert.Throws(typeof(DataVerifier.InvalidNumberException), delegate {
                DataVerifier.VerifyNIP("7511148961"); // bad checksum
            });
        }

        [Test()]
        public void TestVerifyREGON()
        {
            Assert.DoesNotThrow(delegate { // some NIP examples from real CEIDG
                DataVerifier.VerifyREGON("160086724");
                DataVerifier.VerifyREGON("161477740");
                DataVerifier.VerifyREGON("161510710");
                DataVerifier.VerifyREGON("531130409");
                DataVerifier.VerifyREGON("160193213");
                DataVerifier.VerifyREGON("531561026");
            });

            Assert.Throws(typeof(DataVerifier.EmptyFieldException), delegate {
                DataVerifier.VerifyREGON("");
            });

            Assert.Throws(typeof(DataVerifier.IllegalCharactersException), delegate {
                DataVerifier.VerifyREGON("5#@561026");
            });

            Assert.Throws(typeof(DataVerifier.InvalidNumberException), delegate {
                DataVerifier.VerifyREGON("75117112"); // bad length
            });

            Assert.Throws(typeof(DataVerifier.InvalidNumberException), delegate {
                DataVerifier.VerifyREGON("531561021"); // bad checksum
            });
        }

        [Test()]
        public void TestVerifyName()
        {
            Assert.DoesNotThrow(delegate {DataVerifier.VerifyName("Grzegorz Marian");});
            Assert.DoesNotThrow(delegate {DataVerifier.VerifyName("Brzęczyszczykiewicz-Kot");});

            Assert.Throws(typeof(DataVerifier.EmptyFieldException), delegate {
                DataVerifier.VerifyName("");
            });

            Assert.Throws(typeof(DataVerifier.WrongBeginningException), delegate {
                DataVerifier.VerifyName("marcinkiewicz");
            });

            Assert.Throws(typeof(DataVerifier.IllegalCharactersException), delegate {
                DataVerifier.VerifyName("Marcin <3 lol!!!11jeden");
            });
        }

        [Test()]
        public void TestValidProvince()
        {
            Assert.AreEqual(true, DataVerifier.ValidProvince("opolskie"));
            Assert.AreEqual(true, DataVerifier.ValidProvince("świętokrzyskie"));
            Assert.AreEqual(false, DataVerifier.ValidProvince("Montana"));
        }

        [Test()]
        public void TestValidCompanyData()
        {
            Company valid_comp1 = new Company("Apple","7511599616","160058372","Steve","Jobs","opolskie");            
            Company valid_comp2 = new Company("Synthetic Insights","2444224428","144242444","Łukasz","Arczyński","dolnośląskie");
            Company inv_comp_province = new Company("Chatka Puchatka","2444224428","144242444","Kubus","Puchatek","Stumilowy Las");
            Company inv_comp_name = new Company("","2444224428","144242444","Łukasz","Arczyński","dolnośląskie");
            Company inv_comp_owner_name = new Company("Tralala sp. z.o.o.","2444224428","144242444","łukasz","Arc?yński","dolnośląskie");
            Company inv_comp_nip = new Company("Firma123","2444224424","144242444","Łukasz","Arczyński","dolnośląskie");
            Company inv_comp_regon = new Company("Popo","2444224428","144242441","Łukasz","Arczyński","dolnośląskie");

            Assert.AreEqual(true, DataVerifier.ValidCompanyData(valid_comp1));
            Assert.AreEqual(true, DataVerifier.ValidCompanyData(valid_comp2));
            Assert.AreEqual(false, DataVerifier.ValidCompanyData(inv_comp_province));
            Assert.AreEqual(false, DataVerifier.ValidCompanyData(inv_comp_name));
            Assert.AreEqual(false, DataVerifier.ValidCompanyData(inv_comp_owner_name));
            Assert.AreEqual(false, DataVerifier.ValidCompanyData(inv_comp_nip));
            Assert.AreEqual(false, DataVerifier.ValidCompanyData(inv_comp_regon));
        }
    }
}


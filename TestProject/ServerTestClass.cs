using NUnit.Framework;
using System;
using System.Threading;
using System.Collections.Generic;

using System.IO;
using System.Linq;

using CEIDGServer;
using CEIDGClient;
using CEIDGLib;

namespace CEIDGTest
{
    [TestFixture()]
    public class ServerTestClass
    {
        private static string database_path = AppDomain.CurrentDomain.BaseDirectory + "database";
        // This is a helper method that may be ussed as an assertion that
        // any of currently running threads throw the given exception.
        // Useful for testing for exceptions in server thread.
        public void AssertExceptionInAnyThread<Ex>(Action ac, int time_ms = 50){
            bool exception_happened = false;
            UnhandledExceptionEventHandler unhandledExceptionHandler = (object sender, UnhandledExceptionEventArgs exc) =>
            {
                if (exc.ExceptionObject.GetType() == typeof(Ex))
                {
                    exception_happened = true;
                }
            };
            AppDomain.CurrentDomain.UnhandledException += unhandledExceptionHandler;
            ac();
            System.Threading.Thread.Sleep(time_ms);
            AppDomain.CurrentDomain.UnhandledException -= unhandledExceptionHandler;
            Assert.IsTrue(exception_happened);
        }

        [Test()]
        public void StartStop ()
        {
            Server s = new Server (11115, database_path);
            Assert.IsFalse (s.IsRunning ());
            Assert.DoesNotThrow(() => s.Start());
            Assert.IsTrue (s.IsRunning ());
            s.Dispose ();
            Assert.IsFalse (s.IsRunning ());
            Assert.DoesNotThrow(() => s.Start());
            Assert.IsTrue (s.IsRunning ());
            s.Dispose ();
            Assert.IsFalse (s.IsRunning ());
        }
        [Test()]
        public void ConnectionCases()
        {
            Server s = new Server (11116, database_path);
            ServerConnection sc = new ServerConnection (11116);
            Assert.Throws<System.Net.Sockets.SocketException>( () => sc.Start ("127.0.0.1")); // connection refused
            Assert.DoesNotThrow(() => s.Start());
            sc.Close ();
            sc = new ServerConnection (11116);
            Assert.Throws<ServerConnection.InvalidIPException>( () => sc.Start ("foobar") );
            Assert.DoesNotThrow(() =>  sc.Start ("127.0.0.1") );
            sc.Close ();
            s.Dispose ();
        }
        [Test()]
        public void AddCompanyTest()
        {
            System.IO.File.Delete ("database");
            Server s = new Server(11117, database_path);
            s.Start();
            ServerConnection sc = new ServerConnection(11117);
            sc.Start("127.0.0.1");
            Company c = new Company("TestCompany123456","7511702899","531130409","Testing","Tester","opolskie");
            Assert.IsFalse( s.database.Exists("TestCompany123456") );
            sc.RequestAdd(c,ar => {},false);
            System.Threading.Thread.Sleep(100); // Give server thread a chance to catch the add message
            Assert.IsTrue( s.database.Exists("TestCompany123456") );
            Assert.IsNotNull( s.database.FindByName("TestCompany123456") );
            sc.Close();
            s.Dispose();
        }
        [Test()]
        public void MultipleCompaniesSingleName(){
            System.IO.File.Delete ("database");
            Server s = new Server(11118, database_path);
            s.Start ();
            ServerConnection sc = new ServerConnection(11118);
            sc.Start("127.0.0.1");
            Company c = new Company("TestCompany123456","7511702899","531130409","Testing","Tester","opolskie");
            Assert.IsFalse( s.database.Exists("TestCompany123456") );
            sc.RequestAdd(c, result => {});
            System.Threading.Thread.Sleep(100); // Give server thread a chance to catch the add message
            Assert.IsTrue( s.database.Exists("TestCompany123456") );

            // Add the same company again.
            RequestResults.Add ar = RequestResults.Add.SUCCESS;
            AutoResetEvent done_event = new AutoResetEvent(false);
            sc.RequestAdd (c, result => {
                ar = result;
                done_event.Set();
            }, false); // Be careful not to request delegates to be run from GTK thread!
            bool response_received = done_event.WaitOne (200);
            Assert.IsTrue (response_received);
            Assert.IsTrue (ar == RequestResults.Add.NAMEALREADYEXISTS);

            sc.Close ();
            s.Dispose ();
        }
        [Test()]
        public void InvalidDataTest(){
            System.IO.File.Delete (database_path);
            Server s = new Server(11130, database_path);
            s.Start ();
            ServerConnection sc = new ServerConnection(11130);
            sc.Start("127.0.0.1");
            Company c  = new Company("TestCompany123456","0000000001","000000001","Testing","Tester","opolskie");
            Company c2 = new Company("TestCompany123456","7511702899","531130409","Testing","Tester","opolskie");
            Assert.IsFalse( s.database.Exists("TestCompany123456") );

            // Add the invalid company
            RequestResults.Add ar = RequestResults.Add.SUCCESS;
            AutoResetEvent done_event = new AutoResetEvent(false);
            sc.RequestAdd (c, result => {
                ar = result;
                done_event.Set();
            }, false); // Be careful not to request delegates to be run from GTK thread!
            bool response_received = done_event.WaitOne (200);
            Assert.IsTrue (response_received);
            Assert.IsTrue (ar == RequestResults.Add.INVALID);
            // Add the valid company
            sc.RequestAdd (c2, result => {
                ar = result;
                done_event.Set();
            }, false); // Be careful not to request delegates to be run from GTK thread!
            response_received = done_event.WaitOne (200);
            Assert.IsTrue (response_received);
            Assert.IsTrue (ar == RequestResults.Add.SUCCESS);
            sc.Close ();
            s.Dispose ();
        }

        [Test()]
        public void EditCompanyTest()
        {
            System.IO.File.Delete ("database");
            Server s = new Server(11120, database_path);
            s.Start();
            s.disable_priviledge_checks = true;
            ServerConnection sc = new ServerConnection(11120);
            sc.Start("127.0.0.1");
            Company c = new Company("TsetCompany123456","7511702899","531130409","Testing","Tseter","opolskie");
            Assert.IsFalse(s.database.Exists("TsetCompany123456"));
            Assert.IsFalse(s.database.Exists("TestCompany123456"));
            sc.RequestAdd(c,ar => {},false);
            System.Threading.Thread.Sleep(100);
            Assert.IsTrue(s.database.Exists("TsetCompany123456"));
            Assert.IsNotNull(s.database.FindByName("TsetCompany123456"));
            Assert.IsFalse(s.database.Exists("TestCompany123456"));
            Assert.IsNull(s.database.FindByName("TestCompany123456"));
            Company c1 = new Company("TestCompany123456","7511702899","531130409","Testing","Tester","opolskie");
            int id = s.database.FindByName (c.name).id;
            sc.RequestEdit(id, c1, ar => {}, false);
            System.Threading.Thread.Sleep(100);
            Assert.IsTrue(s.database.Exists("TestCompany123456"));
            Assert.IsNotNull(s.database.FindByName("TestCompany123456"));
            Assert.IsFalse(s.database.Exists("TsetCompany123456"));
            Assert.IsNull(s.database.FindByName("TsetCompany123456"));
            sc.Close();
            s.Dispose();
        }

        [Test()]
        public void EditNonExistentCompanyTest()
        {
            System.IO.File.Delete ("database");
            Server s = new Server(11121, database_path);
            s.Start();
            ServerConnection sc = new ServerConnection(11121);
            sc.Start("127.0.0.1");
            Assert.IsFalse(s.database.Exists("TsetCompany123456"));
            Assert.IsFalse(s.database.Exists("TestCompany123456"));
            Company c = new Company("TsetCompany123456","7511702899","531130409","Testing","Tseter","opolskie");
            Company c1 = new Company("TestCompany123456","7511702899","531130409","Testing","Tester","opolskie");
            sc.RequestEdit(c.id, c1, ar => {}, false);
            System.Threading.Thread.Sleep(100);
            Assert.IsFalse(s.database.Exists("TestCompany123456"));
            Assert.IsNull(s.database.FindByName("TestCompany123456"));
            Assert.IsFalse(s.database.Exists("TsetCompany123456"));
            Assert.IsNull(s.database.FindByName("TsetCompany123456"));
            sc.Close();
            s.Dispose();
        }

        [Test()]
        public void EditCompanyNameToExistentTest()
        {
            System.IO.File.Delete ("database");
            Server s = new Server(11173, database_path);
            s.Start();
            ServerConnection sc = new ServerConnection(11173);
            sc.Start("127.0.0.1");
            Company c = new Company("TsetCompany123456","7511702899","531130409","Testing","Tseter","opolskie");
            Company c1 = new Company("TestCompany123456","7222355084","677957618","Testing","Tester","opolskie");
            Assert.IsFalse(s.database.Exists("TsetCompany123456"));
            Assert.IsFalse(s.database.Exists("TestCompany123456"));
            sc.RequestAdd(c,ar => {},false);
            System.Threading.Thread.Sleep(100);
            sc.RequestAdd(c1,ar => {},false);
            System.Threading.Thread.Sleep(100);
            Assert.IsTrue(s.database.Exists("TsetCompany123456"));
            Assert.IsNotNull(s.database.FindByName("TsetCompany123456"));
            Assert.IsTrue(s.database.Exists("TestCompany123456"));
            Assert.IsNotNull(s.database.FindByName("TestCompany123456"));
            int id = s.database.FindByName (c.name).id;
            sc.RequestEdit(id, c1, ar => {}, false);
            System.Threading.Thread.Sleep(100);
            Assert.IsTrue (s.IsRunning ());
            Assert.IsTrue(s.database.Exists("TestCompany123456"));
            Assert.IsNotNull(s.database.FindByName("TestCompany123456"));
            Assert.IsTrue(s.database.Exists("TsetCompany123456"));
            Assert.IsNotNull(s.database.FindByName("TsetCompany123456"));
            sc.Close();
            s.Dispose();
        }

        [Test()]
        public void TestGetCompanyInfo(){
            System.IO.File.Delete ("database");
            Server s = new Server(11119, database_path);
            s.Start ();
            ServerConnection sc = new ServerConnection(11119);
            sc.Start("127.0.0.1");
            
            Company c = new Company("TestCompany123456","7511702899","531130409","Testing","Tester","opolskie");
            Assert.IsFalse( s.database.Exists("TestCompany123456") );
            sc.RequestAdd(c, result => {});

            // Test if requesting info for 1 retrurns the same json
            AutoResetEvent done_event = new AutoResetEvent(false);
            RequestResults.GetCompany gc = RequestResults.GetCompany.SUCCESS;
            Company c2 = null;
            sc.RequestCompanyInfo (1, (result, company) => {
                c2 = company;
                gc = result;
                done_event.Set();
            }, false);
            bool response_received = done_event.WaitOne (200);

            Assert.IsTrue (response_received);
            Assert.AreEqual (gc, RequestResults.GetCompany.SUCCESS);
            Assert.AreEqual (c.ToJson (), c2.ToJson ());

            sc.Close ();
            s.Dispose ();
        }
        [Test()]
        public void ListAll(){
            System.IO.File.Delete ("database");
            Server s = new Server(11119, database_path);
            s.Start ();
            ServerConnection sc = new ServerConnection(11119);
            sc.Start("127.0.0.1");

            Company c;
            c = new Company("T1","7511702899","161510710","Testing","Tester","opolskie");
            Assert.IsFalse( s.database.Exists("T1") );
            sc.RequestAdd(c, result => {});
            c = new Company("T2","7511308837","531130409","Testing","Tester","opolskie");
            Assert.IsFalse( s.database.Exists("T2") );
            sc.RequestAdd(c, result => {});
            c = new Company("T3","7511316162","531561026","Testing","Tester","opolskie");
            Assert.IsFalse( s.database.Exists("T3") );
            sc.RequestAdd(c, result => {});
            c = new Company("T4","7511057588","160193213","Testing","Tester","opolskie");
            Assert.IsFalse( s.database.Exists("T4") );
            sc.RequestAdd(c, result => {});

            // Ask the server to list all companies
            AutoResetEvent done_event = new AutoResetEvent(false);
            List<int> l = null;
            sc.RequestListAll (list => {
                l = list;
                done_event.Set();
            }, false);
            bool response_received = done_event.WaitOne (200);

            Assert.IsTrue (response_received);
            Assert.That (new int[]{1,2,3,4}, Is.EquivalentTo(l));

            sc.Close ();
            s.Dispose ();
        }
        
        [Test()]
        public void TimedSaveTest()
        {
            System.IO.File.Delete (database_path);
            Server s = new Server(11122, database_path);
            s.Start();
            ServerConnection sc = new ServerConnection(11122);
            sc.Start("127.0.0.1");
            Company c = new Company("TestCompany123456","7511702899","531130409","Testing","Tester","opolskie");
            Assert.IsFalse( s.database.Exists("TestCompany123456") );
            sc.RequestAdd(c,ar => {},false);
            s.ChangeSaveInterval (2);
            System.Threading.Thread.Sleep(2500);
            s.database.SaveDatabase();
            string[] saved_companies = File.ReadAllLines(database_path);
            Assert.AreEqual(s.database.companies.ToArray().Select(comp => comp.ToJson()).ToDictionary<string, string>(a=>a), 
                            saved_companies.ToDictionary<string, string>(a=>a));
            sc.Close();
            s.Dispose();
        }
    }
}

 
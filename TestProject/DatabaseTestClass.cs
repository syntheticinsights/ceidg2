using NUnit.Framework;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

using CEIDGServer;
using CEIDGLib;

namespace CEIDGTest
{
    [TestFixture()]
    public class DatabaseTestClass
    {
        private Database database;
        private string filename = "test_db";
        private static Company comp1 = new Company("Apple","2444224428","144242444","Steve","Jobs","opolskie");
        private static Company comp2 = new Company("Chatka Puchatka","7511702899","160086724","Kubus","Puchatek","świętokrzyskie");
        private static Company comp3 = new Company("Synthetic Insights","7511308837","161477740","Łukasz","Arczyński","dolnośląskie");
        private Company nonexistent = new Company("no such company exists","7511316162","161510710","Null","Null","opolskie");
        private List<Company> comps = new List<Company>(new Company[] {comp1, comp2, comp3});

        [TestFixtureSetUp()]
        public void Init()
        {
            FileStream fs;
            fs = File.Create(filename);
            fs.Close();
            StreamWriter file = new StreamWriter(filename);
            for (int i = 0; i < comps.Count; i++)
                file.WriteLine(comps[i].ToJson());
            file.Close();
            database = new Database(filename);
        }

        [Test()]
        public void TestSave()
        {
            database.SaveDatabase();
            string[] saved_companies = File.ReadAllLines(filename);
            Assert.AreEqual(comps.ToArray().Select(comp => comp.ToJson()).ToDictionary<string, string>(a=>a), 
                            saved_companies.ToDictionary<string, string>(a=>a));
        }

        [Test()]
        public void TestAdd()
        {
            Company new_comp = new Company("Macrosoft","7511057588","531130409","Gill","Bates","łódzkie");
            comps.Add(new_comp);
            database.AddCompany(new_comp);
            TestSave();

            Assert.Throws(typeof(Database.CompanyNameAlreadyExistsException), delegate {
                database.AddCompany(comp1);
            });

            Assert.Throws(typeof(Database.CompanyNIPAlreadyExistsException), delegate {
                Company c = new Company(comp1.name + "1", comp1.nip, comp1.regon, comp1.owner_firstname,
                                        comp1.owner_lastname, comp1.province);
                database.AddCompany(c);
            });

            Assert.Throws(typeof(Database.CompanyREGONAlreadyExistsException), delegate {
                Company c = new Company(comp1.name + "2", "7511148960", comp1.regon, comp1.owner_firstname,
                                        comp1.owner_lastname, comp1.province);
                database.AddCompany(c);
            });
        }

        [Test()]
        public void TestRemove()
        {
            comps.Remove(comp2);
            database.RemoveCompany(comp2);
            TestSave();

            Assert.Throws(typeof(Database.CompanyNotFoundException), delegate {
                database.RemoveCompany(nonexistent);
            });
        }

        [Test()]
        public void TestEdit()
        {
            Company new_data = new Company("Apple","2444224428","144242444","Tim","Cook","śląskie");
            comps.Remove(comp1);
            comps.Add(new_data);
            database.EditCompany(database.FindByName(comp1.name).id, new_data);
            TestSave();

            Assert.Throws(typeof(Database.CompanyNameAlreadyExistsException), delegate {
                database.EditCompany(database.FindByName(comp2.name).id, new_data);
            });

            Assert.Throws(typeof(Database.CompanyNIPAlreadyExistsException), delegate {
                Company c = new Company(new_data.name + "1", new_data.nip, new_data.regon, 
                                        new_data.owner_firstname, new_data.owner_lastname, 
                                        new_data.province);
                database.EditCompany(database.FindByName(comp2.name).id, c);
            });

            Assert.Throws(typeof(Database.CompanyREGONAlreadyExistsException), delegate {
                Company c = new Company(new_data.name + "2", "0000000000", new_data.regon, 
                                        new_data.owner_firstname, new_data.owner_lastname, 
                                        new_data.province);
                database.EditCompany(database.FindByName(comp2.name).id, c);
            });
        }

        [Test()]
        public void TestFindByName()
        {
            Assert.AreEqual(comp3.ToJson(), database.FindByName(comp3.name).ToJson());
            Assert.AreEqual(null, database.FindByName(nonexistent.name));
        }

        [Test]
        public void TestFindByProperty()
        {
            Assert.AreEqual(comp3.ToJson(), 
                            database.FindByProperty("owner_firstname", "Łukasz")[0].ToJson());

            Assert.AreEqual(new List<Company>(), database.FindByProperty("name", "nonexistent"));

            Assert.Throws(typeof(Database.BadPropertyException), delegate {
                database.FindByProperty("noexistent property", null);
            });

            Assert.Throws(typeof(Database.WrongTypeException), delegate {
                database.FindByProperty("nip", 'c');
            });
        }

        [TestFixtureTearDown()]
        public void Dispose()
        {
            File.Delete(filename);
        }
    }
}


using System;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Linq;
using System.Reflection;

namespace CEIDGLib
{
    public class DataVerifier
    {
        [Serializable]
        public class IllegalCharactersException : Exception {
            public IllegalCharactersException() : base() {}
            protected IllegalCharactersException
                (SerializationInfo info, StreamingContext ctxt) : base(info, ctxt) {}
        }
        [Serializable]
        public class InvalidNumberException : Exception {
            public InvalidNumberException() : base() {}
            protected InvalidNumberException
                (SerializationInfo info, StreamingContext ctxt) : base(info, ctxt) {}
        }
        [Serializable]
        public class EmptyFieldException : Exception {
            public EmptyFieldException() : base() {}
            protected EmptyFieldException
                (SerializationInfo info, StreamingContext ctxt) : base(info, ctxt) {}
        }
        [Serializable]
        public class WrongBeginningException : Exception {
            public WrongBeginningException() : base() {}
            protected WrongBeginningException
                (SerializationInfo info, StreamingContext ctxt) : base(info, ctxt) {}
        }

        public DataVerifier ()
        {
        }

        public static bool ValidCompanyData(Company comp)
        {
            foreach (PropertyInfo property in typeof(Company).GetProperties())
                if (Object.Equals(property.GetValue (comp, null), null))
                    return false;
            try
            {
                VerifyCompanyName(comp.name);
                VerifyNIP(comp.nip.ToString());
                VerifyREGON(comp.regon.ToString());
                VerifyName(comp.owner_firstname);
                VerifyName(comp.owner_lastname);
            }
            catch (Exception ex)
            {
                if (ex is EmptyFieldException || ex is IllegalCharactersException ||
                    ex is WrongBeginningException || ex is InvalidNumberException)
                    return false;
            }
            if (!ValidProvince(comp.province))
                return false;
            return true;
        }

        public static void VerifyCompanyName(string comp_name)
        {
            if (comp_name == string.Empty)
                throw new EmptyFieldException();
        }

        public static void VerifyNIP(string nip)
        {
            if (nip == string.Empty)
                throw new EmptyFieldException();
            if (!ContainsOnlyDigits(nip))
                throw new IllegalCharactersException();
            if (nip.Length != 10)
                throw new InvalidNumberException();
            int[] weights = new int[] {6, 5, 7, 2, 3, 4, 5, 6, 7, 0};
            int sum = nip.Zip(weights, (d, w) => int.Parse(d.ToString()) * w).Sum();
            if (sum % 11 != int.Parse(nip[9].ToString()))
                throw new InvalidNumberException();
        }

        public static void VerifyREGON(string regon)
        {
            if (regon == string.Empty)
                throw new EmptyFieldException();
            if (!ContainsOnlyDigits(regon))
                throw new IllegalCharactersException();
            if (regon.Length != 9)
                throw new InvalidNumberException();
            int[] weights = new int[] {8, 9, 2, 3, 4, 5, 6, 7, 0};
            int sum = regon.Zip(weights, (d, w) => int.Parse(d.ToString()) * w).Sum();
            if ((sum % 11) % 10 != int.Parse(regon[8].ToString()))
                throw new InvalidNumberException();
        }

        public static void VerifyName(string name)
        {
            if (name == string.Empty)
                throw new EmptyFieldException();
            Regex start = new Regex("^[A-ZĄĆĘŁŃÓŚŹŻ]");
            if (!start.IsMatch(name))
                throw new WrongBeginningException();
            Regex regex = 
                new Regex("^[A-ZĄĆĘŁŃÓŚŹŻa-ząćęłńóśźż -]*$");
            if (!regex.IsMatch(name))
                throw new IllegalCharactersException();
        }

        public static bool ValidProvince(string province)
        {
            string[] valid_provinces = new string[] {
                "dolnośląskie", "kujawsko-pomorskie", "lubelskie", "lubuskie",
                "łódzkie", "małopolskie", "mazowieckie", "opolskie",
                "podkarpackie", "podlaskie", "pomorskie", "śląskie",
                "świętokrzyskie", "warmińsko-mazurskie",
                "wielkopolskie", "zachodniopomorskie"};
            return valid_provinces.Contains(province);
        }

        private static bool ContainsOnlyDigits(string s)
        {
            foreach (char c in s.ToCharArray())
                if (!char.IsDigit(c))
                    return false;
            return true;
        }
    }
}


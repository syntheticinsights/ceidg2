using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace CEIDGLib
{
    public enum Status {WAITING, ACCEPTED, REJECTED};

    [DataContract]
    public class Company
    {
        [DataMember]
        private string name_;
        [DataMember]
        private string nip_;
        [DataMember]
        private string regon_;
        [DataMember]
        private string owner_first_name_;
        [DataMember]
        private string owner_last_name_;
        [DataMember]
        private string province_;

        public int id;

        public string name
        { get { return name_; } }

        public string nip
        { get { return nip_; } }

        public string regon
        { get { return regon_; } }
        
        public string owner_firstname
        { get { return owner_first_name_; } }

        public string owner_lastname
        { get { return owner_last_name_; } }

        public string province
        { get { return province_; } }

        [DataMember]
        public Status status { get; set;}

        public Company(string _name, string _nip, string _regon, string _owner_first_name,
                       string owner_last_name, string _province)
        {
            name_ = _name;
            nip_ = _nip;
            regon_ = _regon;
            owner_first_name_ = _owner_first_name;
            owner_last_name_ = owner_last_name;
            province_ = _province;
            status = Status.WAITING;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static Company CompanyFromJson(string json_string)
        {
            return JsonConvert.DeserializeObject<Company>(json_string);;
        }
    }
}


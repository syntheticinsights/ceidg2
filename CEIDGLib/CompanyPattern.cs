using System;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.IO;
using System.Text;

namespace CEIDGLib
{

    public enum MatchMode{
        Ignore,
        Exactly,
        Contains,
        NotContains
    }

    [DataContract]
    public class CompanyPattern
    {
        private static DataContractJsonSerializer serializer =
            new DataContractJsonSerializer(typeof(CompanyPattern));

        [DataMember]
        public string name  { get; set; }
        [DataMember]
        public string nip  { get; set; }
        [DataMember]
        public string regon  { get; set; }
        [DataMember]
        public string owner_first_name  { get; set; }
        [DataMember]
        public string owner_last_name  { get; set; }
        [DataMember]
        public string province { get; set; }
        [DataMember]
        public Status status { get; set; }
        
        [DataMember]
        public MatchMode use_name = MatchMode.Ignore ,
                         use_nip = MatchMode.Ignore,
                         use_regon = MatchMode.Ignore,
                         use_first_name = MatchMode.Ignore,
                         use_last_name = MatchMode.Ignore,
                         use_province = MatchMode.Ignore;

        public CompanyPattern ()
        {
        }

        
        public string ToJson()
        {
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, this);
            string result = Encoding.UTF8.GetString(ms.ToArray());
            return result;
        }

        public static CompanyPattern CompanyPatternFromJson(string json_string)
        {
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json_string));
            CompanyPattern comp = serializer.ReadObject (ms) as CompanyPattern;
            return comp;
        }

        public bool IsEmpty(){
            return  use_name == MatchMode.Ignore &&
                    use_nip == MatchMode.Ignore &&
                    use_regon == MatchMode.Ignore &&
                    use_first_name == MatchMode.Ignore &&
                    use_last_name == MatchMode.Ignore &&
                    use_province == MatchMode.Ignore;
        }

        public bool MatchesMe(Company c){
            if(   (use_name == MatchMode.Exactly && name != c.name)
               || (use_name == MatchMode.Contains && !c.name.Contains(name))
               || (use_name == MatchMode.NotContains && c.name.Contains(name)) ) return false;
            if(   (use_nip == MatchMode.Exactly && nip != c.nip)
               || (use_nip == MatchMode.Contains && !c.nip.Contains(nip))
               || (use_nip == MatchMode.NotContains && c.nip.Contains(nip)) ) return false;
            if(   (use_province == MatchMode.Exactly && province != c.province)
               || (use_province == MatchMode.Contains && !c.province.Contains(province))
               || (use_province == MatchMode.NotContains && c.province.Contains(province)) ) return false;
            if(   (use_regon == MatchMode.Exactly && regon != c.regon)
               || (use_regon == MatchMode.Contains && !c.regon.Contains(regon))
               || (use_regon == MatchMode.NotContains && c.regon.Contains(regon)) ) return false;
            if(   (use_first_name == MatchMode.Exactly && owner_first_name != c.owner_firstname)
               || (use_first_name == MatchMode.Contains && !c.owner_firstname.Contains(owner_first_name))
               || (use_first_name == MatchMode.NotContains && c.owner_firstname.Contains(owner_first_name)) ) return false;
            if(   (use_last_name == MatchMode.Exactly && owner_last_name != c.owner_lastname)
               || (use_last_name == MatchMode.Contains && !c.owner_lastname.Contains(owner_last_name))
               || (use_last_name == MatchMode.NotContains && c.owner_lastname.Contains(owner_last_name)) ) return false;
            return true;
        }

    }
}


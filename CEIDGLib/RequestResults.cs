using System;

namespace CEIDGLib
{
    public class RequestResults
    {
        public static T Parse<T>(string value)
        {
            return (T)Enum.Parse (typeof(T), value, true);
        }
        public enum Add{ SUCCESS, NAMEALREADYEXISTS, NIPALREADYEXISTS, REGONALREADYEXISTS, INVALID};
        public enum Edit{SUCCESS, NONEXIST, NAMEALREADYEXISTS, NIPALREADYEXISTS, REGONALREADYEXISTS, INSUFFICIENTPRIVILEDGES, INVALID};
        public enum GetCompany{ SUCCESS, NO_SUCH_ID};
        public enum Login{FAILED, NONE, ANON, COMPANY, ADMIN};
    }
}


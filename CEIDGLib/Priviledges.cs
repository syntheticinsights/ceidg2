using System;

namespace CEIDGLib
{
    public class Priviledges
    {
        public enum Level
        {
            None,
            Anonymous,
            Company,
            Admin
        }

        static public Level LevelFromLoginResult(RequestResults.Login result){
            if(result == RequestResults.Login.ADMIN)  return Level.Admin;
            if(result == RequestResults.Login.COMPANY)return Level.Company;
            if(result == RequestResults.Login.ANON)   return Level.Anonymous;
            if(result == RequestResults.Login.NONE)   return Level.None;
            return Level.None;
        }
    }
}


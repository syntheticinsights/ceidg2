#!/bin/sh

mdtool build --configuration:Release ../CEIDG.sln

cp ../CEIDGLib/bin/Release/CEIDGLib.dll libceidg2/opt/CEIDG2/.
cp ../CEIDGLib/bin/Release/Newtonsoft.Json.dll libceidg2/opt/CEIDG2/.
cp ../CEIDGClient/bin/Release/CEIDGClient.exe ceidg2-client/opt/CEIDG2/.
cp ../CEIDGServer/bin/Release/CEIDGServer.exe ceidg2-server/opt/CEIDG2/.

fakeroot dpkg-deb --build libceidg2
fakeroot dpkg-deb --build ceidg2-client
fakeroot dpkg-deb --build ceidg2-server

using System;
using System.Net;
using System.Net.Sockets;

namespace CEIDGServer
{
    // We need this class because .NET is silly and, apparently System.Net.Tcp.Sockets.TcpListener's
    // property 'Active' is not public, and there is no way of avoiding redundant Stop()s or Start(s).
    public class TcpListenerFix : TcpListener
    {
        public TcpListenerFix(IPEndPoint localEP) : base(localEP)
        {
        }
        public TcpListenerFix(IPAddress localaddr, int port) : base(localaddr, port)
        {
        }

        // This is the fix.
        public new bool Active
        {
            get { return base.Active; }
        }
    }
}


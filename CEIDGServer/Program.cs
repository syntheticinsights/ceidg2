using System;

namespace CEIDGServer
{
    class MainClass
    {
        static void Main(string[] args)
        {
            string database_path = AppDomain.CurrentDomain.BaseDirectory + "database";
            int save_interval = 60;
            if (args.Length != 0)
            {
                database_path = args[0];
                if (args.Length != 1)
                    save_interval = Convert.ToInt32(args[1]);
            }
            Server server = new Server (23434, database_path, save_interval);
            server.Start();
            System.Console.WriteLine("blah");
            while (true)
            {
                String s = System.Console.ReadLine();
                if(s == "aaa") break;
                if (s != null) server.SayToAllClients(s);
            }
            server.Dispose ();
        }
    }
}

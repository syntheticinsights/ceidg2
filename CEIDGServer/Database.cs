using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CEIDGLib;
using System.IO;
using System.Runtime.Serialization;

namespace CEIDGServer
{
    public class Database
    {
        [Serializable]
        public class InvalidCompanyException : Exception {
            public InvalidCompanyException() : base() {}
            protected InvalidCompanyException
                (SerializationInfo info, StreamingContext ctxt) : base(info, ctxt) {}
        }
        [Serializable]
        public class CompanyNameAlreadyExistsException : Exception {
            public CompanyNameAlreadyExistsException() : base() {}
            protected CompanyNameAlreadyExistsException
                (SerializationInfo info, StreamingContext ctxt) : base(info, ctxt) {}
        }
        [Serializable]
        public class CompanyNIPAlreadyExistsException : Exception {
            public CompanyNIPAlreadyExistsException() : base() {}
            protected CompanyNIPAlreadyExistsException
                (SerializationInfo info, StreamingContext ctxt) : base(info, ctxt) {}
        }
        [Serializable]
        public class CompanyREGONAlreadyExistsException : Exception {
            public CompanyREGONAlreadyExistsException() : base() {}
            protected CompanyREGONAlreadyExistsException
                (SerializationInfo info, StreamingContext ctxt) : base(info, ctxt) {}
        }
        [Serializable]
        public class CompanyNotFoundException : Exception { 
            public CompanyNotFoundException() : base() {}
            protected CompanyNotFoundException
                (SerializationInfo info, StreamingContext ctxt) : base(info, ctxt) {}
        }

        [Serializable]
        public class BadPropertyException : Exception { 
            public BadPropertyException() : base() {}
            protected BadPropertyException
                (SerializationInfo info, StreamingContext ctxt) : base(info, ctxt) {}
        }

        [Serializable]
        public class WrongTypeException : Exception { 
            public WrongTypeException() : base() {}
            protected WrongTypeException
                (SerializationInfo info, StreamingContext ctxt) : base(info, ctxt) {}
        }

        private Dictionary<string, Company> comps;
        private Dictionary<int, Company> comps_with_ids;
        private string location;
        private object base_lock = new object();
        private int largest_used_id;

        public List<Company> companies
        {
            get
            {
                return comps.Values.ToList();
            }
        }

        public Database(string path)
        {
            location = path;
            comps = new Dictionary<string, Company>();
            comps_with_ids = new Dictionary<int, Company>();
            if (!File.Exists(location))
            {
                FileStream fs;
                fs = File.Create(location);
                fs.Close();
                Console.WriteLine("\"{0}\" file created.", path);
            }
            LoadDatabase();
        }

        private void LoadDatabase()
        {
            int current_id = 0;
            List<string> invalid_entries = new List<string>();
            lock (base_lock) {
                foreach (string s in File.ReadLines(location)) 
                {
                    Company company;
                    try {
                        company = Company.CompanyFromJson(s);
                    } catch (Newtonsoft.Json.JsonReaderException) {
                        invalid_entries.Add(s);
                        continue;
                    }
                    if (!DataVerifier.ValidCompanyData(company) || 
                        comps.ContainsKey(company.name) || 
                        FindByProperty("nip", company.nip).Count != 0 ||
                        FindByProperty("regon", company.regon).Count != 0)
                    {
                        invalid_entries.Add(s);
                        continue;
                    }
                    company.id = current_id;
                    comps.Add (company.name, company);
                    comps_with_ids.Add(company.id, company);
                    current_id++;
                }
                largest_used_id = current_id;
            }
            if (invalid_entries.Count != 0)
            {
                Console.WriteLine("---------------------------------------------------");
                Console.WriteLine("The following database entries have been identified as invalid:\n");
                foreach (string s in invalid_entries)
                    Console.WriteLine(s);
                Console.WriteLine("---------------------------------------------------");
            }
        }

        public void AddCompany(Company comp)
        {
            lock(base_lock) {
                if (!DataVerifier.ValidCompanyData(comp))
                    throw new InvalidCompanyException();
                else if (comps.ContainsKey(comp.name))
                    throw new CompanyNameAlreadyExistsException();
                else if (FindByProperty("nip", comp.nip).Count != 0)
                    throw new CompanyNIPAlreadyExistsException();
                else if (FindByProperty("regon", comp.regon).Count != 0)
                    throw new CompanyREGONAlreadyExistsException();
                else
                {
                    comp.id = largest_used_id + 1;
                    comps.Add(comp.name, comp);
                    comps_with_ids.Add(comp.id, comp);
                    largest_used_id++;
                }
            }
        }

        public void RemoveCompany(Company comp)
        {
            lock(base_lock) {
                if (comps.ContainsKey(comp.name))
                {
                    int id = comps[comp.name].id;
                    comps.Remove(comp.name);
                    comps_with_ids.Remove(id);
                }
                else
                    throw new CompanyNotFoundException();
            }
        }

        public void EditCompany(int id, Company new_comp)
        {
            lock(base_lock) {
                if (comps.ContainsKey(new_comp.name) && 
                    comps_with_ids [id].name != new_comp.name)
                    throw new CompanyNameAlreadyExistsException();
                else if (FindByProperty("nip", new_comp.nip).Count != 0 && 
                         comps_with_ids [id].nip != new_comp.nip)
                    throw new CompanyNIPAlreadyExistsException();
                else if (FindByProperty("regon", new_comp.regon).Count != 0 && 
                         comps_with_ids [id].regon != new_comp.regon)
                    throw new CompanyREGONAlreadyExistsException();
                else
                    try {
                        string name = comps_with_ids [id].name;
                        comps.Remove(name);
                        comps.Add(new_comp.name, new_comp);
                        comps_with_ids[id] = new_comp;
                    } catch(KeyNotFoundException){
                        throw new CompanyNotFoundException ();
                    }
            }
        }

        public void SaveDatabase()
        {
            StreamWriter file = new StreamWriter(location);
            lock (base_lock) {
                foreach (Company c in comps.Values)
                    file.WriteLine (c.ToJson());
            }
            file.Close();
        }

        public Company FindByName(string name)
        {
            lock (base_lock) {
                if (!Exists (name))
                    return null;
                return comps [name];
            }
        }

        public Company FindByID(int id)
        {
            lock (base_lock) {
                if (!Exists (id))
                    return null;
                return comps_with_ids[id];
            }
        }

        public List<Company> FindByProperty(string property_name, object value)
        {
            lock (base_lock) {
                PropertyInfo property = typeof(Company).GetProperty (property_name);
                if (property == null)
                    throw new BadPropertyException();

                Type type_of_property = property.PropertyType;
                Type type_of_value = value.GetType ();
                if (type_of_property != type_of_value)
                    throw new WrongTypeException();

                return companies.Where (c => Object.Equals (property.GetValue (c, null), value)).ToList ();
            }
        }

        public bool Exists(string name)
        {
            lock (base_lock) {
                return comps.ContainsKey (name);
            }
        }

        public bool Exists(int id)
        {
            lock (base_lock) {
                return comps_with_ids.ContainsKey (id);
            }
        }

        public List<int> ListMatching(CompanyPattern cp){
            List<int> result = new List<int> ();
            foreach (KeyValuePair<int, Company> p in comps_with_ids) {
                int id = p.Key;
                Company c = p.Value;
                if (cp.MatchesMe (c))
                    result.Add (id);
            }
            return result;
        }

        public List<int> ListAll(){
            lock (base_lock) {
                return comps_with_ids.Keys.ToList ();
            }
        }
    }
}


﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Web;
using Newtonsoft.Json;
using System.Collections.Generic;
using CEIDGLib;

namespace CEIDGServer
{
    public class ClientConnection : ClientInterface
    {
        TcpClient client;
        NetworkStream data_stream;
        Thread the_thread;

        public ClientConnection(TcpClient _client, Server server) : base(server)
        {
            client = _client;
            data_stream = client.GetStream();
            the_thread = new Thread(new ThreadStart(this.ThreadMain));
            the_thread.Start();
        }
        ~ClientConnection()
        {
            System.Console.WriteLine("Client {0} destroyed.", GetName());
            if(client != null)
                Kill();
        }
        public void Kill()
        {
            the_thread.Abort();
            client.Close();
            the_thread.Join();
            client = null;
        }
        private void ThreadMain()
        {
            System.Console.WriteLine("Connection estabilished: {0}.", GetName());
            HandleIncomingData();
        }
        private String GetName()
        {
            if (client == null)
                return "[???]";
            return "[" + client.Client.RemoteEndPoint.ToString() + "]";
        }
        private void HandleIncomingData()
        {
            byte[] buffer = new byte[1024];
            String message = "";
            while (true)
            {
                if(data_stream == null) return;
                while (!data_stream.DataAvailable) {
                    Thread.Sleep (20);
                    if (data_stream == null) return;
                }
                int read = data_stream.Read(buffer, 0, buffer.Length);
                message = String.Concat(message, System.Text.Encoding.UTF8.GetString(buffer, 0, read));
                if (message.Contains(";"))
                {
                    String[] messages = message.Split(';');
                    int n = messages.Length;
                    for (int i = 0; i < n - 1; i++)
                    {
                        InterpretMessage(messages[i]);
                    }
                    message = messages[n - 1];
                }
            }
        }
        private void InterpretMessage(String s)
        {
            Console.Out.WriteLine("Got \"{0}\" from {1}.", s , GetName());
            String[] data = s.Split('|');
            int requestid, companyID;
            Company c;
            switch (data[0].Trim().ToLower())
            {
                case "say":
                    base.Say (data [1]);
                    break;
                case "add":
                    requestid = Int32.Parse (data [1]);
                    Company company = Company.CompanyFromJson(data[2]);
                    RequestResults.Add addresult = base.AddCompany (company);
                    SendData (String.Format ("result|{0}|{1};",requestid,addresult));
                    break;
                case "edit":
                    requestid = Int32.Parse (data [1]);
                    companyID = Int32.Parse (data [2]);
                    Company edited = Company.CompanyFromJson(data[3]);
                    RequestResults.Edit editresult = base.Edit (companyID, edited);
                    SendData (String.Format ("result|{0}|{1};",requestid,editresult));
                    break;
                case "listall":
                    requestid = Int32.Parse (data [1]);
                    List<int> list = base.ListAll ();
                    SendData (String.Format ("listall|{0}|{1};",requestid, JsonConvert.SerializeObject(list) ));
                    break;
                case "getcompany":
                    requestid = Int32.Parse (data [1]);
                    companyID = Int32.Parse (data [2]);
                    RequestResults.GetCompany getresult = base.GetCompany (companyID, out c);
                    SendData (String.Format ("getcompany|{0}|{1}+{2};",requestid,getresult,(c==null)?"":c.ToJson()));
                    break;
                case "listmatching":
                    requestid = Int32.Parse (data [1]);
                    CompanyPattern cp = CompanyPattern.CompanyPatternFromJson (data [2]);
                    list = base.ListMatching (cp);
                    SendData (String.Format ("listmatching|{0}|{1};", requestid, JsonConvert.SerializeObject (list)));
                    break;
                case "accept":
                    companyID = Int32.Parse (data [1]);
                    base.AcceptCompany (companyID);
                    break;
                case "reject":
                    companyID = Int32.Parse (data [1]);
                    base.RejectCompany (companyID);
                    break;
                case "setwaiting":
                    companyID = Int32.Parse (data [1]);
                    base.SetWaiting (companyID);
                    break;
                case "login":
                    requestid = Int32.Parse (data [1]);
                    RequestResults.Login loginresult = base.Login (data [2], data [3]);
                    SendData (String.Format ("result|{0}|{1};", requestid, loginresult));
                    break;
                case "logout":
                    base.Logout ();
                    break;
                default:
                    System.Console.WriteLine("Client {0} send an unrecognized command '{1}'.", GetName(), data[0]);
                    break;
            }
        }
        public void SendData(String s)
        {
            if (data_stream == null || client == null) return;
            try{
                Console.Out.WriteLine("Sending \"{0}\" to {1}",s,GetName());
                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(s);
                data_stream.Write(buffer,0,buffer.Length);
            }catch(Exception ex){
                if(ex is System.IO.IOException || ex is ObjectDisposedException || ex is SocketException){
                    Console.WriteLine("Sending failed, apparently the connection is lost.");
                    Kill ();
                }else throw;
            }
        }
        public override void SayToClient(String s)
        {
            SendData(String.Format("say|{0};",s));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace CEIDGServer
{
    public class Server : IDisposable
    {
        private TcpListenerFix tcplistener;

        public Database database
        {
            get
            {
                return _database;
            }
        }
        private Database _database;

        public Authenticator authenticator;

        int PORT;
        Thread server_thread;
        Thread save_thread;
        bool running = false;
        int save_interval;
        public bool IsRunning(){
            return running;
        }

        public bool disable_priviledge_checks = false;
        List<ClientConnection> clients = new List<ClientConnection>();

        public Server(int port, string database_path, int interval = 60) {
            PORT = port;
            _database = new Database(database_path);
            authenticator = new Authenticator (_database);
            save_interval = interval;
        }

        public void Start()
        {
            StartWithoutThread ();
            StartThread ();
            StartSaveThread();
        }
        public void StartWithoutThread()
        {
            if (running) return;
            tcplistener = new TcpListenerFix(IPAddress.Any, PORT);
            tcplistener.Start();
            running = true;
        }
        public void StartThread(){
            server_thread = new Thread(new ThreadStart(this.ServerThreadMain));
            server_thread.IsBackground = true;
            server_thread.Start();
            System.Threading.Thread.Sleep (10);
        }
        public void StartSaveThread(){
            save_thread = new Thread(new ThreadStart(this.SaveThread));
            save_thread.IsBackground = true;
            save_thread.Start();
            System.Threading.Thread.Sleep (10);
        }

        public void Dispose()
        {
            if (!running) return;
            clients.Clear();
            server_thread.Abort();
            System.Threading.Thread.Sleep (10);
            server_thread.Join();
            server_thread = null;
            tcplistener.Stop ();
            running = false;
        }

        private void ServerThreadMain()
        {
            while (true) {
                try {
                    if(!tcplistener.Active) tcplistener.Start();
                    TcpClient new_client = tcplistener.AcceptTcpClient ();
                    ClientConnection client_connection = new ClientConnection (new_client, this);
                    clients.Add (client_connection);
                } catch (System.Net.Sockets.SocketException ex) {
                    if (ex.ErrorCode == 10004) { // WSAEINTR (interrupted blocking call)
                        continue;
                    }
                    throw;
                } catch (Exception) {
                    if(tcplistener.Active) tcplistener.Stop ();
                    throw;
                }
            }
        }
        private void SaveThread()
        {
            while (true) {
                System.Threading.Thread.Sleep (save_interval * 1000);
                TrySave();
            }
        }
        public void SayToAllClients(String s)
        {
            foreach (ClientConnection client in clients)
            {
                client.SayToClient(s);
            }
        }
        public void ChangeSaveInterval(int interval)
        {
            save_interval = interval;
        }
        public void TrySave()
        {
            try {
                database.SaveDatabase();
                Console.WriteLine("Database saved.");
            } catch(Exception) {
                Console.Error.WriteLine("Oops. Database not saved!");
                //throw;
            }
        }
    }
}

using System;
using System.Runtime.Serialization;

using CEIDGLib;

namespace CEIDGServer
{
    public class Authenticator
    {
        [Serializable]
        public class InvalidCredentialsException : Exception {
            public InvalidCredentialsException() : base() {}
            protected InvalidCredentialsException
                (SerializationInfo info, StreamingContext ctxt) : base(info, ctxt) {}
        }
        
        private Database db;
        public Authenticator (Database d)
        {
            db = d;
        }



        public Priviledges.Level Login(string username, string password){
            if(username == "" && password == "") return Priviledges.Level.None;
            if(username == "ANON" && password == "ANON") return Priviledges.Level.Anonymous;
            if(username == "admin" && password == "admin") return Priviledges.Level.Admin;

            // TODO: login as company owner (nip + password)

            throw new InvalidCredentialsException ();
        }
    }
}


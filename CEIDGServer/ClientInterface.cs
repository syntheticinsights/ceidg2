using System;
using CEIDGLib;
using System.Collections.Generic;

namespace CEIDGServer
{
    public class ClientInterface
    {

        private Server server;
        Priviledges.Level priviledge_level = Priviledges.Level.None;

        public ClientInterface (Server s)
        {
            server = s;
        }

        public void Say(string text){
            System.Console.WriteLine("A client says {0}." , text);
        }

        public RequestResults.Add AddCompany(Company company){
            try {
                server.database.AddCompany(company);
                SayToClient(String.Format("Company \"{0}\" has been added.", company.name));
                return RequestResults.Add.SUCCESS;
            } catch(Database.CompanyNameAlreadyExistsException){
                return RequestResults.Add.NAMEALREADYEXISTS;
            } catch(Database.CompanyNIPAlreadyExistsException){
                return RequestResults.Add.NIPALREADYEXISTS;
            } catch(Database.CompanyREGONAlreadyExistsException){
                return RequestResults.Add.REGONALREADYEXISTS;
            } catch(Database.InvalidCompanyException){
                return RequestResults.Add.INVALID;
            } catch(Exception) {
                SayToClient(String.Format("Error has occured! Company \"{0}\" has not been added.", company.name));
                throw;
            }
        }
        public RequestResults.Edit Edit(int id, Company edited){
            if (!server.disable_priviledge_checks && priviledge_level != Priviledges.Level.Admin) {
                return RequestResults.Edit.INSUFFICIENTPRIVILEDGES;
            }
            try {
                server.database.EditCompany(id, edited);
                SayToClient(String.Format("Company \"{0}\" has been changed to \"{1}\".", id, edited.name));
                return RequestResults.Edit.SUCCESS;
            } catch(Database.CompanyNameAlreadyExistsException){
                return RequestResults.Edit.NAMEALREADYEXISTS;
            } catch(Database.CompanyNIPAlreadyExistsException){
                return RequestResults.Edit.NIPALREADYEXISTS;
            } catch(Database.CompanyREGONAlreadyExistsException){
                return RequestResults.Edit.REGONALREADYEXISTS;
            } catch(Database.InvalidCompanyException){
                return RequestResults.Edit.INVALID;
            } catch(Exception) {
                SayToClient(String.Format("Error has occured! Company \"{0}\" has not been changed.", id));
                throw;
            }
        }

        public List<int> ListAll(){
            return server.database.ListAll ();
        }

        public List<int> ListMatching(CompanyPattern cp){
            return server.database.ListMatching (cp);
        }

        public RequestResults.GetCompany GetCompany(int id, out Company c){
            c = server.database.FindByID (id);
            if (c != null) {
                return RequestResults.GetCompany.SUCCESS;
            } else {
                return RequestResults.GetCompany.NO_SUCH_ID;
            }
        }
        
        public void AcceptCompany(int id){
            if (!server.disable_priviledge_checks && priviledge_level != Priviledges.Level.Admin) return;
            server.database.FindByID (id).status = Status.ACCEPTED;
        }
        public void RejectCompany(int id){
            if (!server.disable_priviledge_checks && priviledge_level != Priviledges.Level.Admin) return;
            server.database.FindByID (id).status = Status.REJECTED;
        }
        public void SetWaiting(int id){
            if (!server.disable_priviledge_checks && priviledge_level != Priviledges.Level.Admin) return;
            server.database.FindByID (id).status = Status.WAITING;
        }

        public RequestResults.Login Login(string username, string password){
            try{
                priviledge_level = server.authenticator.Login (username, password);
                if (priviledge_level == Priviledges.Level.Admin) {
                    return RequestResults.Login.ADMIN;
                }else if (priviledge_level == Priviledges.Level.Company) {
                    return RequestResults.Login.COMPANY;
                }else if (priviledge_level == Priviledges.Level.Anonymous) {
                    return RequestResults.Login.ANON;
                }else{
                    return RequestResults.Login.NONE;
                }
            }catch(Authenticator.InvalidCredentialsException){
                return RequestResults.Login.FAILED;
            }
        }
        public void Logout(){
            priviledge_level = Priviledges.Level.None;
        }

        public virtual void SayToClient(string s){
            // will be overridden
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gtk;
using CEIDGLib;

namespace CEIDGClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Gtk.Application.Init ();
            ConnectWindow cw = new ConnectWindow ();
            cw.Show ();
            Gtk.Application.Run (); // passes control to GTK
            return;
        }
    }
}

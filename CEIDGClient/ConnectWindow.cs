using System;
using CEIDGLib;

namespace CEIDGClient
{
    public partial class ConnectWindow : Gtk.Window
    {
        bool login_in_progress = false;

        public ConnectWindow () : 
                base(Gtk.WindowType.Toplevel)
        {
            this.Build ();
            foreach(Gtk.Widget e in this.table2)
                e.Accessible.Name = e.Name;
        }

        protected void OnDeleteEvent(object o, Gtk.DeleteEventArgs a){
            Gtk.Application.Quit();
        }

        protected void OnGtkButtonQuitClicked (object sender, EventArgs e)
        {
            Gtk.Application.Quit();
        }

        protected void OnGtkRadioNoLoginToggled (object sender, EventArgs e)
        {
            UpdateCredentialSensitivity ();
        }

        protected void OnGtkRadioLoginToggled (object sender, EventArgs e)
        {
            UpdateCredentialSensitivity ();
        }

        private void UpdateCredentialSensitivity(){
            gtkFrameCredentials.Sensitive = gtkRadioLogin.Active;
        }

        protected void OnGtkButtonConnectClicked (object sender, EventArgs e)
        {
            if (login_in_progress)
                return; // This refuses simultanous multiple login requests

            ServerConnection sc = new ServerConnection (23434);
            try{
                sc.Start( gtkEntryServerIP.Text );
            }catch(System.Net.Sockets.SocketException ex){
                gtkLabelFeedback.Text = "Błąd połączenia: " + ex.Message;
                return;
            }catch(ServerConnection.InvalidIPException){
                gtkLabelFeedback.Text = "Błędny adres IP serwera";
                return;
            }
            
            // Perform login.
            string username = "ANON", password = "ANON";
            if (gtkRadioLogin.Active) {
                username = gtkEntryUsername.Text;
                password = gtkEntryPassword.Text;
            }

            login_in_progress = true;
            sc.Login (username, password, (RequestResults.Login result) => {
                if (result == RequestResults.Login.FAILED) {
                    gtkLabelFeedback.Text = "Nieprawidłowe dane logowania.";
                    login_in_progress = false;
                    return;
                } else {
                    SwitchToMainWindow (sc, Priviledges.LevelFromLoginResult (result));
                    login_in_progress = false;
                }
            });

        }

        private void SwitchToMainWindow(ServerConnection sc, Priviledges.Level level){
            // Open up the main window
            MainWindow mw = new MainWindow (sc);
            mw.SwitchPriviledgeLevel (level);
            mw.Show ();
            this.Hide ();
        }

        private void ResetErrorMessage(){
            gtkLabelFeedback.Text = "";
        }

        protected void OnGtkEntryServerIPChanged (object sender, EventArgs e)
        {
            ResetErrorMessage ();
        }

        protected void OnGtkEntryUsernameChanged (object sender, EventArgs e)
        {
            ResetErrorMessage ();
        }

        protected void OnGtkEntryPasswordChanged (object sender, EventArgs e)
        {
            ResetErrorMessage ();
        }
    }
}


using System;

namespace CEIDGClient
{
    public partial class SearchWindow : Gtk.Window
    {
        //ServerConnection sc;
        public SearchWindow (ServerConnection connection) : 
                base(Gtk.WindowType.Toplevel)
        {
            //sc = connection;
            this.Build ();
        }

        protected void OnGtkButtonCancelClicked (object sender, EventArgs e)
        {
            this.Destroy();
        }
    }
}
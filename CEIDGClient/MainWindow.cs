using System;
using System.Collections.Generic;
using System.Linq;
using CEIDGLib;

namespace CEIDGClient
{
    public partial class MainWindow : Gtk.Window
    {
        ServerConnection sc;
        Gtk.ListStore treeModel;
        struct IDRefPair{
            public int id;
            public Gtk.TreeRowReference reference;
        };
        List<IDRefPair> currently_displayed_ids = new List<IDRefPair>();
        CompanyInfoWidget company_info_widget = new CompanyInfoWidget();
        bool programically_switching_notebook_page = false; // Used as a workaround for gtk not providing a way
            // to recognize whether page switch was done by the user, or by the app                                                    

        public MainWindow (ServerConnection connection) : 
                base(Gtk.WindowType.Toplevel)
        {
            sc = connection;
            this.Build ();
            //////////
            gtkTreeView.Accessible.Name = "gtkTreeView";
            foreach(Gtk.Widget e in this.table2)
                e.Accessible.Name = e.Name;
            //////////
            gtkVboxAddCompany.PackStart(company_info_widget);
            company_info_widget.Visible = true;
            company_info_widget.on_add_clicked += OnAddCompanyClicked;
            company_info_widget.on_cancel_clicked += OnCancelEditingCompanyClicked;
            company_info_widget.on_save_edit_clicked += OnSaveCompanyEditsClicked;
            InitTreeView ();
            SwitchNotebookPage (0);
            UpdateFilter ();
        }

        private void SwitchNotebookPage(int page){
            programically_switching_notebook_page = true;
            gtkNotebookMain.Page = page;
            programically_switching_notebook_page = false;
        }

        private void InitTreeView(){
            treeModel = new Gtk.ListStore (typeof(int), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string));
            
            Gtk.TreeViewColumn treeCollumnName = new Gtk.TreeViewColumn ();
            treeCollumnName.Title = "Nazwa";
            Gtk.TreeViewColumn treeCollumnNIP = new Gtk.TreeViewColumn ();
            treeCollumnNIP.Title = "NIP";
            Gtk.TreeViewColumn treeCollumnFirstName = new Gtk.TreeViewColumn ();
            treeCollumnFirstName.Title = "Imię";
            Gtk.TreeViewColumn treeCollumnLastName = new Gtk.TreeViewColumn ();
            treeCollumnLastName.Title = "Nazwisko";
            Gtk.TreeViewColumn treeCollumnStatus = new Gtk.TreeViewColumn ();
            treeCollumnStatus.Title = "Status";
            
            gtkTreeView.InsertColumn (treeCollumnName,0);
            gtkTreeView.InsertColumn (treeCollumnNIP,1);
            gtkTreeView.InsertColumn (treeCollumnFirstName,2);
            gtkTreeView.InsertColumn (treeCollumnLastName,3);
            gtkTreeView.InsertColumn (treeCollumnStatus,4);
            
            Gtk.CellRendererText treeCRTname = new Gtk.CellRendererText ();
            treeCollumnName.PackStart (treeCRTname,false);
            treeCollumnName.AddAttribute (treeCRTname, "text", 1);
            Gtk.CellRendererText treeCRTnip = new Gtk.CellRendererText ();
            treeCollumnNIP.PackStart (treeCRTnip,false);
            treeCollumnNIP.AddAttribute (treeCRTnip, "text", 2);
            Gtk.CellRendererText treeCRTfirstname = new Gtk.CellRendererText ();
            treeCollumnFirstName.PackStart (treeCRTfirstname,false);
            treeCollumnFirstName.AddAttribute (treeCRTfirstname, "text", 3);
            Gtk.CellRendererText treeCRTlastname = new Gtk.CellRendererText ();
            treeCollumnLastName.PackStart (treeCRTlastname,false);
            treeCollumnLastName.AddAttribute (treeCRTlastname, "text", 4);
            Gtk.CellRendererText treeCRTstatus = new Gtk.CellRendererText ();
            treeCollumnStatus.PackStart (treeCRTstatus,false);
            treeCollumnStatus.AddAttribute (treeCRTstatus, "text", 5);

            gtkTreeView.Model = treeModel;
        }
        public void SwitchPriviledgeLevel(Priviledges.Level level){
            switch (level) {
                case Priviledges.Level.Admin:
                    gtkLabelUserName.Text = "Zalogowano w trybie administratora.";
                    break;
                case Priviledges.Level.Company:
                    gtkLabelUserName.Text = "Zalogowano w trybie firmy.";
                    break;
                case Priviledges.Level.Anonymous:
                    gtkLabelUserName.Text = "Zalogowano w trybie anonimowym.";
                    break;
                case Priviledges.Level.None:
                    gtkLabelUserName.Text = "Wylogowano.";
                    break;
            }
            gtkButtonAccept.Sensitive = (level == Priviledges.Level.Admin);
            gtkButtonReject.Sensitive = (level == Priviledges.Level.Admin);
            gtkButtonEdit.Sensitive   = (level == Priviledges.Level.Admin);
        }

        private void UpdateFilter(){
            CompanyPattern cp = new CompanyPattern ();
            if (gtkEntryFilterName.Text     != "") { cp.name             = gtkEntryFilterName.Text;      cp.use_name       = MatchMode.Contains; }
            if (gtkEntryFilterNIP.Text      != "") { cp.nip              = gtkEntryFilterNIP.Text;       cp.use_nip        = MatchMode.Contains; }
            if (gtkEntryFilterREGON.Text    != "") { cp.regon            = gtkEntryFilterREGON.Text;     cp.use_regon      = MatchMode.Contains; }
            if (gtkEntryFilterFirstName.Text!= "") { cp.owner_first_name = gtkEntryFilterFirstName.Text; cp.use_first_name = MatchMode.Contains; }
            if (gtkEntryFilterLastName.Text != "") { cp.owner_last_name  = gtkEntryFilterLastName.Text;  cp.use_last_name  = MatchMode.Contains; }
            if (gtkEntryFilterProvince.Text != "") { cp.province         = gtkEntryFilterProvince.Text;  cp.use_province   = MatchMode.Contains; }

            Action<List<int>> on_list_received = delegate(List<int> q){
                treeModel.Clear ();
                currently_displayed_ids.Clear ();
                q.ForEach (delegate(int i) {
                    Gtk.TreeIter ti = treeModel.Append ();
                    Gtk.TreePath tp = treeModel.GetPath (ti);
                    currently_displayed_ids.Add (new IDRefPair {
                        id = i,
                        reference=new Gtk.TreeRowReference (treeModel, tp)
                    });
                });
                RefreshCompanySummaries ();
            };

            if (cp.IsEmpty ()) {
                Console.WriteLine ("cp is empty");
                sc.RequestListAll (on_list_received);
            } else {
                Console.WriteLine ("cp is not empty");
                sc.RequestListMatching (cp, on_list_received);
            }
        }
        private void RefreshCompanySummaries(){
            currently_displayed_ids.ForEach (delegate (IDRefPair idr) {
                sc.RequestCompanyInfo(idr.id, delegate(RequestResults.GetCompany res, Company c) {
                    if(res == RequestResults.GetCompany.NO_SUCH_ID){
                        Console.WriteLine("Failed to get info of company {0}, server claims it does not exist.");
                        // TODO: Remove this row from treeview.
                    }else if(res == RequestResults.GetCompany.SUCCESS){
                        UpdateCompanyRow(idr.id,idr.reference,c);
                    }else{
                        // ???
                    }
                });
            });
        }
        private void UpdateCompanyRow(int id, Gtk.TreeRowReference tr, Company c){
            Gtk.TreePath tp = tr.Path;
            Gtk.TreeIter ti = new Gtk.TreeIter();
            if(tp == null){
                Console.WriteLine ("Not updating a tree row that no longer exists.");
            }else{
                treeModel.GetIter (out ti, tp);
                treeModel.SetValues (ti, id, c.name, c.nip, c.owner_firstname, c.owner_lastname, c.status.ToString());
            }
        }

        protected void OnGtkButtonQuitClicked (object sender, EventArgs e)
        {
            sc.Close ();
            Gtk.Application.Quit ();
        }
        protected void OnDeleteEvent(object o, Gtk.DeleteEventArgs a){
            sc.Close ();
            Gtk.Application.Quit ();
        }

        protected void OnGtkButtonLogoutClicked (object sender, EventArgs e)
        {
            sc.Logout ();
            sc.Close();
            ConnectWindow cw = new ConnectWindow();
            cw.Show();
            this.Hide ();
        }

        protected void OnGtkButtonEditCompanyClicked (object sender, EventArgs e)
        {
            //RefreshCompanySummaries();
            Gtk.TreeSelection ts = gtkTreeView.Selection;
            if (ts.CountSelectedRows () == 1) 
            {
                Gtk.TreeIter ti;
                ts.GetSelected (out ti);
                int companyID = (int)treeModel.GetValue (ti, 0);

                sc.RequestCompanyInfo(companyID, delegate(RequestResults.GetCompany res, Company c) {
                    if(res == RequestResults.GetCompany.SUCCESS){
                        company_info_widget.SetCompany(c);
                        company_info_widget.editted_id = companyID;
                        company_info_widget.SetDisplayMode(CompanyInfoWidget.CompanyDisplayModes.DISPLAY_MODE_EDIT);
                        SwitchNotebookPage(2);
                    }else{
                        Console.WriteLine("Failed to get info of company {0}, server claims it does not exist.");
                    }
                });

            }
        }

        protected void OnCancelEditingCompanyClicked(){
            SwitchNotebookPage(0);
        }

        protected void OnSaveCompanyEditsClicked(){
            Company c = company_info_widget.ConstructCompany ();
            KeyValuePair<bool, string> verification = VerifyData(c);

            if (verification.Key)
            {
                string errors = verification.Value.Remove(verification.Value.Length - 1);
                company_info_widget.SetFeedback(errors);
            }
            else
            {
                sc.RequestEdit(company_info_widget.editted_id, c, (RequestResults.Edit ar) => {
                    if(ar == RequestResults.Edit.INVALID){
                        company_info_widget.SetFeedback("Niepoprawne dane");
                    }else if(ar == RequestResults.Edit.NAMEALREADYEXISTS){
                        company_info_widget.SetFeedback("Firma o podanej nazwie już istnieje");
                    }else if(ar == RequestResults.Edit.NIPALREADYEXISTS){
                        company_info_widget.SetFeedback("Firma o podanym NIP już istnieje");
                    }else if(ar == RequestResults.Edit.REGONALREADYEXISTS){
                        company_info_widget.SetFeedback("Firma o podanym REGON już istnieje");
                    }else if(ar == RequestResults.Edit.INSUFFICIENTPRIVILEDGES){
                        company_info_widget.SetFeedback("Nie posiadasz uprawnień by dokonać edycji!");
                    }else if(ar == RequestResults.Edit.SUCCESS){
                        company_info_widget.SetFeedback("Zmiany zapisane");
                        sc.SetWaiting(company_info_widget.editted_id);
                    }else{
                        // ?
                    }
                });
                UpdateFilter ();
            }
        }

        protected void OnAddCompanyClicked (){
            Company c = company_info_widget.ConstructCompany ();
            KeyValuePair<bool, string> verification = VerifyData(c);

            if (verification.Key)
            {
                string errors = verification.Value.Remove(verification.Value.Length - 1);
                company_info_widget.SetFeedback(errors);
            }
            else
            {
                sc.RequestAdd(c, (RequestResults.Add ar) => {
                    if(ar == RequestResults.Add.INVALID){
                        company_info_widget.SetFeedback("Niepoprawne dane");
                    }else if(ar == RequestResults.Add.NAMEALREADYEXISTS){
                        company_info_widget.SetFeedback("Firma o podanej nazwie już istnieje");
                    }else if(ar == RequestResults.Add.NIPALREADYEXISTS){
                        company_info_widget.SetFeedback("Firma o podanym NIP już istnieje");
                    }else if(ar == RequestResults.Add.REGONALREADYEXISTS){
                        company_info_widget.SetFeedback("Firma o podanym REGON już istnieje");
                    }else if(ar == RequestResults.Add.SUCCESS){
                        company_info_widget.SetFeedback("Firma dodana do bazy");
                    }else{
                        // ?
                    }
                });
                UpdateFilter ();
            }
        }

        private KeyValuePair<bool, string> VerifyData(Company c)
        {
            bool wrong_data = false;
            string errors = string.Empty;

            try  {
                DataVerifier.VerifyCompanyName(c.name);
            } catch (DataVerifier.EmptyFieldException) {                
                errors += "Nazwa firmy jest wymagana!\n";
                wrong_data = true;
            }
            try  {
                DataVerifier.VerifyNIP(c.nip);
            } catch (DataVerifier.EmptyFieldException) {                
                errors += "NIP jest wymagany!\n";
                wrong_data = true;
            } catch (DataVerifier.IllegalCharactersException) {                
                errors += "NIP musi być liczbą!\n";
                wrong_data = true;
            } catch (DataVerifier.InvalidNumberException) {
                errors += "Błędny NIP!\n";
                wrong_data = true;
            }
            try  {
                DataVerifier.VerifyREGON(c.regon);
            } catch (DataVerifier.EmptyFieldException) {                
                errors += "REGON jest wymagany!\n";
                wrong_data = true;
            } catch (DataVerifier.IllegalCharactersException) {            
                errors += "REGON musi być liczbą!\n";
                wrong_data = true;
            } catch (DataVerifier.InvalidNumberException) {
                errors += "Błędny REGON!\n";
                wrong_data = true;
            }
            try  {
                DataVerifier.VerifyName(c.owner_firstname);
            } catch (DataVerifier.EmptyFieldException) {                
                errors += "Imię jest wymagane!\n";
                wrong_data = true;
            } catch (DataVerifier.WrongBeginningException) {                
                errors += "Imię powinno zaczynać się wielką literą!\n";
                wrong_data = true;
            } catch (DataVerifier.IllegalCharactersException) {                
                errors += "Imię powinno zawierać tylko litery, spacje lub myślniki!\n";
                wrong_data = true;
            }
            try  {
                DataVerifier.VerifyName(c.owner_lastname);
            } catch (DataVerifier.EmptyFieldException) {                
                errors += "Nazwisko jest wymagane!\n";
                wrong_data = true;
            } catch (DataVerifier.WrongBeginningException) {                
                errors += "Nazwisko powinno zaczynać się wielką literą!\n";
                wrong_data = true;
            } catch (DataVerifier.IllegalCharactersException) {                
                errors += "Nazwisko powinno zawierać tylko litery, spacje lub myślniki!\n";
                wrong_data = true;
            }

            return new KeyValuePair<bool, string>(wrong_data, errors);
        }

        protected void CheckForNumbersOnEntry(object o, Gtk.TextInsertedArgs args)
        {
            bool illegalCharactersFound = false;
            foreach(char c in args.Text)
                if (!char.IsDigit(c))
                    illegalCharactersFound = true;
            if (illegalCharactersFound) {
                string s = ((Gtk.Entry)o).Text;
                IEnumerable<char> res = s.TakeWhile (c => char.IsDigit (c));
                s = string.Join ("", res.ToArray ());
                ((Gtk.Entry)o).Text = s; // tutaj wysyłany jest sygnał TextInserted,
            } // z samymi poprawnymi znakami, więc else w
            // tym miejscu i tak się wykona
        } 

        protected void CheckForNumbersOnEntryAndSync(object o, Gtk.TextInsertedArgs args)
        {
            CheckForNumbersOnEntry(o, args);
            UpdateFilter ();
        }

        protected void UpdateFilterOnInsert(object o, Gtk.TextInsertedArgs args){
            UpdateFilter ();
        }
        protected void UpdateFilterOnRemove(object o, Gtk.TextDeletedArgs args){
            UpdateFilter ();
        }

        protected void OnGtkNotebookMainSwitchPage (object o, Gtk.SwitchPageArgs args)
        {
            if (programically_switching_notebook_page)
                return; // ignore
            if (args.PageNum == 2) {
                 // Switched to company info
                Gtk.TreeSelection ts = gtkTreeView.Selection;
                if (ts.CountSelectedRows () == 1) {
                    Gtk.TreeIter ti;
                    ts.GetSelected (out ti);
                    int companyID = (int)treeModel.GetValue (ti, 0);

                    sc.RequestCompanyInfo (companyID, delegate(RequestResults.GetCompany res, Company c) {
                        if (res == RequestResults.GetCompany.SUCCESS) {
                            company_info_widget.SetCompany (c);
                            company_info_widget.editted_id = companyID;
                            company_info_widget.SetDisplayMode (CompanyInfoWidget.CompanyDisplayModes.DISPLAY_MODE_INFO);
                        } else {
                            Console.WriteLine ("Failed to get info of company {0}, server claims it does not exist.");
                            company_info_widget.SetInvalidMessage("Wybrana firma nie istnieje.");
                        }
                    });

                } else {
                    company_info_widget.SetInvalidMessage ("Nie wybrano firmy!");
                }
            }
        }

        protected void ClearFilter(){
            gtkEntryFilterName.Text = "";
            gtkEntryFilterNIP.Text = "";
            gtkEntryFilterREGON.Text = "";
            gtkEntryFilterProvince.Text = "";
            gtkEntryFilterFirstName.Text = "";
            gtkEntryFilterLastName.Text = "";
            UpdateFilter ();
        }

        protected void OnGtkButtonClearFilterClicked (object sender, EventArgs e)
        {
            ClearFilter ();
        }

        protected void OnGtkButtonRefreshClicked (object sender, EventArgs e)
        {
            UpdateFilter ();
        }

        protected void OnGtkButtonAcceptClicked (object sender, EventArgs e)
        {
            Gtk.TreeSelection ts = gtkTreeView.Selection;
            if (ts.CountSelectedRows () == 1) {
                Gtk.TreeIter ti;
                ts.GetSelected (out ti);
                int id = (int)treeModel.GetValue (ti, 0);
                sc.AcceptCompany (id);
                RefreshCompanySummaries ();
            }
        }

        protected void OnGtkButtonRejectClicked (object sender, EventArgs e)
        {
            Gtk.TreeSelection ts = gtkTreeView.Selection;
            if (ts.CountSelectedRows () == 1) {
                Gtk.TreeIter ti;
                ts.GetSelected (out ti);
                int id = (int)treeModel.GetValue (ti, 0);
                sc.RejectCompany (id);
                RefreshCompanySummaries ();
            }
        }

        protected void OnGtkButtonCreateCompanyClicked (object sender, EventArgs e)
        {
            company_info_widget.SetCompany(null);
            company_info_widget.SetDisplayMode(CompanyInfoWidget.CompanyDisplayModes.DISPLAY_MODE_ADD);
            SwitchNotebookPage(2);
        }

    }
}
using System;
using System.Linq;
using System.Collections.Generic;
using CEIDGLib;

namespace CEIDGClient
{
    
    public delegate void ButtonClickHandler();

    [System.ComponentModel.ToolboxItem(true)]
    public partial class CompanyInfoWidget : Gtk.Bin
    {
        public enum CompanyDisplayModes{
            DISPLAY_MODE_INVALID,  // no company selected
            DISPLAY_MODE_INFO,     // just display uneditable data
            DISPLAY_MODE_ADD,      // to create a new company
            DISPLAY_MODE_EDIT      // allow editing company
        }
        private Dictionary<string, int> voivodeships = new Dictionary<string, int>()
        {
            {"dolnośląskie", 0}, {"kujawsko-pomorskie", 1}, {"lubelskie", 2}, {"lubuskie", 3}, 
            {"łódzkie", 4}, {"małopolskie", 5}, {"mazowieckie", 6}, {"opolskie", 7},
            {"podkarpackie", 8}, {"podlaskie", 9}, {"pomorskie", 10}, {"śląskie", 11},
            {"świętokrzyskie", 12}, {"warmińsko-mazurskie", 13}, {"wielkopolskie", 14}, {"zachodniopomorskie", 15}
        };

        CompanyDisplayModes display_mode = CompanyDisplayModes.DISPLAY_MODE_INVALID;
        Company comp = null;
        
        public event ButtonClickHandler on_add_clicked;
        public event ButtonClickHandler on_cancel_clicked;
        public event ButtonClickHandler on_save_edit_clicked;

        public int editted_id;

        public CompanyInfoWidget ()
        {
            this.Build ();
            foreach(Gtk.Widget e in this.vbox2)
                e.Accessible.Name = e.Name;
            foreach(Gtk.Widget e in this.table1)
                e.Accessible.Name = e.Name;

            gtkEntryNIP.MaxLength = 10;
            gtkEntryREGON.MaxLength = 9;

            Pango.FontDescription feedback_font = new Pango.FontDescription();
            feedback_font.Family = "Sans";
            feedback_font.Size = Convert.ToInt32(14 * Pango.Scale.PangoScale);
            feedback_font.Weight = Pango.Weight.Bold;
            Gdk.Color feedback_color = new Gdk.Color(245, 20, 20);
            gtkLabelFeedback.ModifyFont(feedback_font);
            gtkLabelFeedback.ModifyFg(Gtk.StateType.Normal, feedback_color);

            UpdateDisplayedData ();
        }
        public void SetCompany(Company c){
            comp = c;
            UpdateDisplayedData ();
        }
        public void SetDisplayMode(CompanyDisplayModes mode){
            display_mode = mode;
            gtkLabelFeedback.Text = "";
            UpdateDisplayedData ();
        }
        public void SetInvalidMessage(string msg){
            display_mode = CompanyDisplayModes.DISPLAY_MODE_INVALID;
            gtkLabelInvalidMessage.Text = msg;
            UpdateDisplayedData ();
        }
        private void UpdateDisplayedData(){
            if (display_mode == CompanyDisplayModes.DISPLAY_MODE_INVALID) {
                gtkNotebook.Page = 1;
                return;
            }
            gtkNotebook.Page = 0;

            if (display_mode == CompanyDisplayModes.DISPLAY_MODE_INFO) {
                gtkEntryCompanyName.IsEditable = false;
                gtkEntryNIP.IsEditable = false;
                gtkEntryREGON.IsEditable = false;
                gtkEntryFirstName.IsEditable = false;
                gtkEntryLastName.IsEditable = false;
                gtkEntryProvince.Sensitive = false;
            } else {
                gtkEntryCompanyName.IsEditable = true;
                gtkEntryNIP.IsEditable = true;
                gtkEntryREGON.IsEditable = true;
                gtkEntryFirstName.IsEditable = true;
                gtkEntryLastName.IsEditable = true;
                gtkEntryProvince.Sensitive = true;
            }
            gtkButtonBoxAdding.Visible = (display_mode == CompanyDisplayModes.DISPLAY_MODE_ADD);
            gtkButtonBoxEditing.Visible = (display_mode == CompanyDisplayModes.DISPLAY_MODE_EDIT);
            
            if (comp == null) {
                gtkEntryCompanyName.Text = "";
                gtkEntryNIP.Text = "";
                gtkEntryREGON.Text = "";
                gtkEntryFirstName.Text = "";
                gtkEntryLastName.Text = "";
                gtkEntryProvince.Active = 0;
            } else {
                gtkEntryCompanyName.Text = comp.name;
                gtkEntryNIP.Text = comp.nip;
                gtkEntryREGON.Text = comp.regon;
                gtkEntryLastName.Text = comp.owner_lastname;
                gtkEntryFirstName.Text = comp.owner_firstname;
                gtkEntryProvince.Active = voivodeships[comp.province];
                if (display_mode == CompanyDisplayModes.DISPLAY_MODE_INFO)
                    gtkLabelHeader.Markup = "<b>Wyświetlanie danych o firmie: " + comp.name + "</b>";
                else if(display_mode == CompanyDisplayModes.DISPLAY_MODE_EDIT)
                    gtkLabelHeader.Markup = "<b>Edytowanie danych firmy: " + comp.name + "</b>";
            }
            if (display_mode == CompanyDisplayModes.DISPLAY_MODE_ADD)
                gtkLabelHeader.Markup = "<b>Podaj dane nowej firmy</b>";

        }
        protected void CheckForNumbersOnEntry(object o, Gtk.TextInsertedArgs args)
        {
            bool illegalCharactersFound = false;
            foreach(char c in args.Text)
                if (!char.IsDigit(c))
                    illegalCharactersFound = true;
            if (illegalCharactersFound) {
                string s = ((Gtk.Entry)o).Text;
                IEnumerable<char> res = s.TakeWhile (c => char.IsDigit (c));
                s = string.Join ("", res.ToArray ());
                ((Gtk.Entry)o).Text = s; // tutaj wysyłany jest sygnał TextInserted,
            } // z samymi poprawnymi znakami, więc else w
            // tym miejscu i tak się wykona
        } 

        protected void OnGtkButtonAddClicked (object sender, EventArgs e)
        {
            on_add_clicked ();
        }

        protected void OnGtkButtonSaveClicked (object sender, EventArgs e)
        {
            on_save_edit_clicked ();
        }

        protected void OnGtkButtonCancelClicked (object sender, EventArgs e)
        {
            on_cancel_clicked ();
        }

        public Company ConstructCompany(){
            return new Company (gtkEntryCompanyName.Text, gtkEntryNIP.Text, gtkEntryREGON.Text, gtkEntryFirstName.Text, gtkEntryLastName.Text, gtkEntryProvince.ActiveText);  
        }
        public void SetFeedback(string feedback){
            gtkLabelFeedback.Text = feedback;
        }

    }
}


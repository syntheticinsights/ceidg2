﻿using System;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using System.Collections.Generic;
using Newtonsoft.Json;
using CEIDGLib;

namespace CEIDGClient
{
    public class ServerConnection
    {
        TcpClient tcpclient;
        NetworkStream data_stream;
        Thread the_thread;
        int PORT;
        public bool started = false;
        static int requestid = 0;
        Dictionary<int,Action<string>> response_handlers = new Dictionary<int, Action<string>>();


        public ServerConnection(int port)
        {   PORT = port;

        }
        ~ServerConnection()
        {
            Close();
        }

        public class InvalidIPException : Exception{
            public InvalidIPException() : base(){}
            public InvalidIPException(string msg) : base(msg){}
        }

        // Launches the connection
        public void Start(String IP){
            tcpclient = new TcpClient();
            System.Net.IPAddress ipaddr;
            try{
                ipaddr = IPAddress.Parse(IP.Trim());
            }catch(System.FormatException ex){
                throw new InvalidIPException(ex.Message);
            }
            tcpclient.Connect(ipaddr,PORT);
            data_stream = tcpclient.GetStream();
            the_thread = new Thread(new ThreadStart(this.ThreadMain));
            the_thread.Start();
            started = true;
        }
        public void Close(){
            if (started) {
                started = false;
                tcpclient.Close ();
                the_thread.Abort ();
                the_thread.Join ();
            }
        }
        private void ThreadMain()
        {
            try{
            byte[] buffer = new byte[1024];
            String message = "";
            while(true){
                while(!data_stream.DataAvailable && started)
                    Thread.Sleep(20);
                int read = data_stream.Read(buffer,0,buffer.Length);
                message = String.Concat(message,System.Text.Encoding.UTF8.GetString(buffer,0,read));
                if(message.Contains(";")){
                    String[] messages = message.Split(';');
                    int n = messages.Length;
                    for(int i = 0; i < n-1; i++){
                        String t = messages[i];
                        // The following may need wrapping in a defered delagate if connected to a synchronous UI
                        InterpretMessage(t);
                    }
                    message = messages[n-1];
                }
            }
            }catch(System.IO.IOException ex){
                if (ex.InnerException is System.Threading.ThreadAbortException) {
                    // Thread aborting causes this exception. This is not any exceptional situation
                    // it's just a quick way to cleanly stop a running thread. Therefore there is
                    // nothing that should be done as a reaction to this exception. If it is left
                    // unhandled, the framework quietly ignores it. However, test suites are too
                    // restrictive and catch it, treating it as a possible error (while it is not).
                    // So hereby we ignore this particular exception.
                } else {
                    throw;
                }
            }
        }
        private void SendData(String s){
            if(data_stream == null || !started) return;
            try{
                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(s);
                data_stream.Write(buffer,0,buffer.Length);
            }catch(Exception ex){
                if(ex is System.IO.IOException || ex is ObjectDisposedException || ex is SocketException){
                    Console.WriteLine("Sending failed, apparently the connection is lost.");
                    Close ();
                }else throw;
            }
        }
        private void InterpretMessage(String s)
        {
            System.Console.WriteLine("Got message {0} from server.", s);
            String[] data = s.Split('|');
            switch (data[0].Trim().ToLower())
            {
            case "say":
                System.Console.WriteLine("Server says {0}", data[1]);
                break;
            case "result":
            case "listall":
            case "getcompany":
            case "listmatching":
                int id = Int32.Parse (data [1]);
                response_handlers [id] (data [2]); // Call equivalent response handler
                response_handlers.Remove (id);
                break;
            default:
                System.Console.WriteLine("Unknown message from server: {0}.", s);
                break;
            }
        }
        public void SayToServer(String s)
        {    
            Console.Out.WriteLine ("saying {0} to server", s);
            SendData(String.Format("say|{0};",s));
        }
        public void AcceptCompany(int id){
            SendData(String.Format("accept|{0};",id));
        }
        public void RejectCompany(int id){
            SendData(String.Format("reject|{0};",id));
        }

        public void SetWaiting(int id)
        {
            SendData(String.Format("setwaiting|{0};",id));
        }
        public void Logout()
        {
            SendData(String.Format("logout;"));
        }
        public void RequestAdd(Company company, Action<RequestResults.Add> action, bool call_action_from_gtk_thread = true)
        {
            Action<string> response_action = delegate(string s){
                RequestResults.Add ar = RequestResults.Parse<RequestResults.Add>(s);
                if(call_action_from_gtk_thread){
                    Gtk.Application.Invoke(delegate{action(ar);});
                }else{
                    action(ar);
                }
            };
            requestid++;
            response_handlers[requestid] = response_action;
            SendData(String.Format("add|{0}|{1};", requestid, company.ToJson()));
        }

        public void RequestEdit(int id, Company edited, Action<RequestResults.Edit> action, bool call_action_from_gtk_thread = true)
        {
            Action<string> response_action = delegate(string s){
                RequestResults.Edit ar = RequestResults.Parse<RequestResults.Edit>(s);
                if(call_action_from_gtk_thread){
                    Gtk.Application.Invoke(delegate{action(ar);});
                }else{
                    action(ar);
                }
            };
            requestid++;
            response_handlers[requestid] = response_action;
            SendData(String.Format("edit|{0}|{1}|{2};", requestid, id, edited.ToJson()));
        }

        public void RequestListAll(Action<List<int>> action, bool call_action_from_gtk_thread = true)
        {
            Action<string> response_action = delegate(string s){
                List<int> l = JsonConvert.DeserializeObject<List<int>>(s);
                if(call_action_from_gtk_thread){
                    Gtk.Application.Invoke(delegate{action(l);});
                }else{
                    action(l);
                }
            };
            requestid++;
            response_handlers [requestid] = response_action;
            SendData(String.Format("listall|{0};",requestid));
        }
        public void RequestCompanyInfo(int companyID, Action<RequestResults.GetCompany,Company> action, bool call_action_from_gtk_thread = true)
        {
            Action<string> response_action = delegate(string s){
                String[] q = s.Split("+".ToCharArray(),2);
                RequestResults.GetCompany cr = RequestResults.Parse<RequestResults.GetCompany>(q[0]);
                Company c = null;
                if(cr == RequestResults.GetCompany.SUCCESS) c = Company.CompanyFromJson(q[1]);
                if(call_action_from_gtk_thread){
                    Gtk.Application.Invoke(delegate{action(cr,c);});
                }else{
                    action(cr,c);
                }
            };
            requestid++;
            response_handlers [requestid] = response_action;
            SendData(String.Format("getcompany|{0}|{1};",requestid,companyID));
        }
        public void RequestListMatching(CompanyPattern cp, Action<List<int>> action, bool call_action_from_gtk_thread = true)
        {
            Action<string> response_action = delegate(string s){
                List<int> l = JsonConvert.DeserializeObject<List<int>>(s);
                if(call_action_from_gtk_thread){
                    Gtk.Application.Invoke(delegate{action(l);});
                }else{
                    action(l);
                }
            };
            requestid++;
            response_handlers [requestid] = response_action;
            SendData(String.Format("listmatching|{0}|{1};",requestid,cp.ToJson()));
        }
        public void Login(string username, string password, Action<RequestResults.Login> action, bool call_action_from_gtk_thread = true)
        {
            Action<string> response_action = delegate(string s){
                RequestResults.Login l = RequestResults.Parse<RequestResults.Login>(s);
                if(call_action_from_gtk_thread){
                    Gtk.Application.Invoke(delegate{action(l);});
                }else{
                    action(l);
                }
            };
            requestid++;
            response_handlers [requestid] = response_action;
            SendData(String.Format("login|{0}|{1}|{2};",requestid,username,password));
        }
    }
}
#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
from dogtail import tree
from dogtail.utils import run
from dogtail.tree import SearchError
from TestUtils import *

import time

def TestEdit():
  with CEIDGClient() as client, CEIDGServer() as server:
    app = client.get_app();
    app.childNamed(u'Zaloguj').click()
    app.textentry(u'gtkEntryUsername').typeText(u'admin')
    app.childNamed(u'gtkEntryPassword').typeText(u'admin')
    app.button(u'Połącz').click()

    wrong_company_name = u'Dmeland'
    correct_company_name = u'Demland'
    used_company_name = u'TakaFirmaJuzIstnieje'
    used_nip = '4242142444'
    correct_nip = '0000000000'
    used_regon = '930192790'

    entry_namefilter = app.textentry(u'gtkEntryFilterName')
    assert entry_namefilter is not None
    entry_namefilter.click()
    entry_namefilter.typeText(wrong_company_name)

    wrongname = app.childNamed(wrong_company_name)
    assert wrongname is not None
    wrongname.click()

    editbutton = app.button(u'Edytuj dane')
    assert editbutton is not None
    editbutton.click()

    entry_name = app.textentry(u'gtkEntryCompanyName')
    assert entry_name is not None
    entry_name.click()
    entry_name.keyCombo("<Control>a")
    entry_name.typeText(used_company_name)

    savebutton = app.button(u'Zapisz zmiany')
    assert savebutton is not None
    savebutton.click()

    feedback = app.childNamed(u'gtkLabelFeedback')
    assert feedback.text == 'Firma o podanej nazwie już istnieje'

    entry_name.click()
    entry_name.keyCombo("<Control>a")
    entry_name.typeText(correct_company_name)

    entry_nip = app.textentry(u'gtkEntryNIP')
    assert entry_nip is not None
    entry_nip.click()
    entry_nip.keyCombo("<Control>a")
    entry_nip.typeText(used_nip)

    savebutton.click()

    assert feedback.text == 'Firma o podanym NIP już istnieje'

    entry_nip.click()
    entry_nip.keyCombo("<Control>a")
    entry_nip.typeText(correct_nip)

    entry_regon = app.textentry(u'gtkEntryREGON')
    assert entry_regon is not None
    entry_regon.click()
    entry_regon.keyCombo("<Control>a")
    entry_regon.typeText(used_regon)

    savebutton.click()

    assert feedback.text == 'Firma o podanym REGON już istnieje'

    time.sleep(1)

    quitbutton = app.button(u'Zakończ')
    assert quitbutton is not None
    quitbutton.click()
  print("Test successful.")

if __name__ == "__main__":
  TestEdit()

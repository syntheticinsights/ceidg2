#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
from dogtail import tree
from dogtail.utils import run
from dogtail.tree import SearchError
from TestUtils import *

import time

def TestAddCompany():
  with CEIDGClient() as client, CEIDGServer() as server:
    app = client.get_app();
    connectbutton = app.button(u'Połącz')
    connectbutton.click()

    company_name = u'TakaFirmaJuzIstnieje'
    company_name2 = u'TakaFirmaJeszczeNieIstnieje'
    nip = '7511148960'
    nip2 = '1111111111'
    regon = '531561026'

    entry_namefilter = app.textentry(u'gtkEntryFilterName')
    assert entry_namefilter is not None
    entry_namefilter.click()
    entry_namefilter.typeText(company_name)

    usedname = app.childNamed(company_name)
    assert usedname is not None
    usedname.click()

    clearbutton = app.button(u'Czyść filtr')
    assert clearbutton is not None
    clearbutton.click()

    entry_nipfilter = app.textentry(u'gtkEntryFilterNIP')
    assert entry_nipfilter is not None
    entry_nipfilter.click()
    entry_nipfilter.typeText(nip)

    usednip = app.childNamed(nip)
    assert usednip is not None
    usednip.click()

    clearbutton.click()

    entry_regonfilter = app.textentry(u'gtkEntryFilterREGON')
    assert entry_regonfilter is not None
    entry_regonfilter.click()
    entry_regonfilter.typeText(regon)

    usedname = app.childNamed(company_name)
    assert usedname is not None
    usedname.click()

    company_data = app.childNamed('Dane firmy')
    assert company_data is not None
    company_data.click()

    entry_regon = app.textentry(u'gtkEntryREGON')
    assert entry_regon is not None
    entry_regon.click()
    entry_regon.keyCombo("<Control>a")

    browsetab = app.tab(u'Przeglądaj')
    assert browsetab is not None
    browsetab.click()

    addtab = app.button(u'Utwórz firmę')
    assert addtab is not None
    addtab.click()

    entry_name = app.textentry(u'gtkEntryCompanyName')
    assert entry_name is not None
    entry_name.click()
    entry_name.typeText(company_name)

    entry_nip = app.textentry(u'gtkEntryNIP')
    assert entry_nip is not None
    entry_nip.click()
    entry_nip.typeText(nip)

    entry_regon = app.textentry(u'gtkEntryREGON')
    assert entry_regon is not None
    entry_regon.click()
    entry_regon.typeText(regon)

    entry_ownerfirst = app.textentry(u'gtkEntryFirstName')
    assert entry_ownerfirst is not None
    entry_ownerfirst.click()
    entry_ownerfirst.typeText(u'Marek')

    entry_ownerlast = app.textentry(u'gtkEntryLastName')
    assert entry_ownerlast is not None
    entry_ownerlast.click()
    entry_ownerlast.typeText(u'Markowski')

    addbutton = app.button(u'Dodaj')
    assert addbutton is not None
    addbutton.click()

    feedback = app.childNamed(u'gtkLabelFeedback')
    assert feedback.text == 'Firma o podanej nazwie już istnieje'

    entry_name.click()
    entry_name.keyCombo("<Control>a")
    entry_name.typeText(company_name2)

    addbutton.click()

    assert feedback.text == 'Firma o podanym NIP już istnieje'

    entry_nip.click()
    entry_nip.keyCombo("<Control>a")
    entry_nip.typeText(nip2)

    addbutton.click()

    assert feedback.text == 'Firma o podanym REGON już istnieje'

    time.sleep(1)

    quitbutton = app.button(u'Zakończ')
    assert quitbutton is not None
    quitbutton.click()
  print("Test successful.")

if __name__ == "__main__":
  TestAddCompany()

#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
from dogtail import tree
from dogtail.utils import run
from dogtail.tree import SearchError
from TestUtils import *

import time

def TestAddCompany():
  with CEIDGClient() as client, CEIDGServer() as server:
    app = client.get_app();
    connectbutton = app.button(u'Połącz')
    connectbutton.click()

    company_name = u'AUTO MYJNIA'
    nip = '8961212836'
    regon = '930205328'

    entry_namefilter = app.textentry(u'gtkEntryFilterName')
    assert entry_namefilter is not None
    entry_namefilter.click()
    entry_namefilter.typeText(company_name)

    newcomp = None
    try:
      newcomp = app.childNamed(company_name)
    except SearchError:
      pass
    assert newcomp is None

    addtab = app.button(u'Utwórz firmę')
    assert addtab is not None
    addtab.click()

    entry_name = app.textentry(u'gtkEntryCompanyName')
    assert entry_name is not None
    entry_name.click()
    entry_name.typeText(company_name)

    entry_nip = app.textentry(u'gtkEntryNIP')
    assert entry_nip is not None
    entry_nip.click()
    entry_nip.typeText(nip)

    entry_regon = app.textentry(u'gtkEntryREGON')
    assert entry_regon is not None
    entry_regon.click()
    entry_regon.typeText(regon)

    entry_ownerfirst = app.textentry(u'gtkEntryFirstName')
    assert entry_ownerfirst is not None
    entry_ownerfirst.click()
    entry_ownerfirst.typeText(u'Marek')

    entry_ownerlast = app.textentry(u'gtkEntryLastName')
    assert entry_ownerlast is not None
    entry_ownerlast.click()
    entry_ownerlast.typeText(u'Marczewski')

    addbutton = app.button(u'Dodaj')
    assert addbutton is not None
    addbutton.click()

    feedback = app.childNamed(u'gtkLabelFeedback')
    assert feedback.text == 'Firma dodana do bazy'

    browsetab = app.tab(u'Przeglądaj')
    assert browsetab is not None
    browsetab.click()

    newcomp = app.childNamed(company_name)
    assert newcomp is not None
    newcomp.click()

    status = app.childNamed(u'Status')
    company = app.childNamed(company_name)
    treeview = app.childNamed(u'gtkTreeView')
    companies = treeview.children
    status_field_index = companies.index(status)
    company_index = companies.index(company)
    company_status = companies[company_index + status_field_index]

    assert company_status.text == 'WAITING'
    company_status.click()

    time.sleep(1)

    quitbutton = app.button(u'Zakończ')
    assert quitbutton is not None
    quitbutton.click()
  print("Test successful.")

if __name__ == "__main__":
  TestAddCompany()

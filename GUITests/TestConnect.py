#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
from dogtail import tree
from dogtail.utils import run
from dogtail.tree import SearchError
from TestUtils import *

def TestConnect():
	with CEIDGClient() as client, CEIDGServer() as server:
		app = client.get_app();
		connectbutton = app.button(u'Połącz')
		connectbutton.click()
		logoutbutton = app.button(u'Wyloguj')
		assert logoutbutton is not None
		logoutbutton.click()
		quitbutton = app.button(u'Zakończ')
		assert quitbutton is not None
		quitbutton.click()
	print("Test successful.")

if __name__ == "__main__":
	TestConnect()

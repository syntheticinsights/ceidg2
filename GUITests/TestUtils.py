
import os
import subprocess
from os import environ
from dogtail import tree
from dogtail.utils import run
from dogtail.tree import SearchError
from time import sleep

class CEIDGServer:
	def __init__(self):
		self.server = None
	def __enter__(self):
		self.server = run_server()
		return self
	def __exit__(self, type, value, traceback):
		send_sigterm(self.server)

class CEIDGClient:
	def __init__(self):
		self.client = None
	def __enter__(self):
		self.client = run_client()
		return self
	def __exit__(self, type, value, traceback):
		send_sigterm(self.client)
	def get_app(self):
		print('Searching for the app in AT-SPI tree. For some reason, this may take a long time.')
		try:
			return tree.root.application('CEIDGClient',False)
		except SearchError:
			return None


def run_client():

	environ['LANG']='pl_PL.UTF-8'
	clientpath = "../CEIDGClient/bin/Debug/CEIDGClient.exe"

	if not os.path.exists(clientpath):
		print("You must build the project first.")
		return False

	proc = subprocess.Popen([clientpath], shell=False)

	sleep(0.5)
	print("Started the client. Do not use the desktop until the test is completed.")

	return proc

def run_server():

	environ['LANG']='pl_PL.UTF-8'
	serverpath = "../CEIDGServer/bin/Debug/CEIDGServer.exe"

	if not os.path.exists(serverpath):
		print("You must build the project first.")
		return False

	proc = subprocess.Popen([serverpath, "test_database", "1000000"], shell=False)

	sleep(0.5)

	return proc



def send_sigterm(proc):
	subprocess.call(["kill", "-15", "%d" % proc.pid])
	proc.wait()
	if proc.poll() is None:
		print("The sigtermed process is still running, apparently.")

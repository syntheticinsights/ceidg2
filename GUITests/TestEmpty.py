#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
from dogtail import tree
from dogtail.utils import run
from dogtail.tree import SearchError
from TestUtils import *

def TestEmpty():
	with CEIDGClient() as client:
		app = client.get_app();
		quitbutton = app.button(u'Zakończ')
		quitbutton.click()
		assert client.get_app() is None
	print("Test successful.")

if __name__ == "__main__":
	TestEmpty()

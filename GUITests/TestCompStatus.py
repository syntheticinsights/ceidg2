#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
from dogtail import tree
from dogtail.utils import run
from dogtail.tree import SearchError
from TestUtils import *

import time

def TestAddCompany():
  with CEIDGClient() as client, CEIDGServer() as server:
    app = client.get_app();
    app.childNamed(u'Zaloguj').click()
    app.textentry(u'gtkEntryUsername').typeText(u'admin')
    app.childNamed(u'gtkEntryPassword').typeText(u'admin')
    app.button(u'Połącz').click()

    company_name = u'Firma do zatwierdzenia'

    entry_namefilter = app.textentry(u'gtkEntryFilterName')
    assert entry_namefilter is not None
    entry_namefilter.click()
    entry_namefilter.typeText(company_name)

    newentry = app.childNamed(company_name)
    assert newentry is not None
    newentry.click()

    status = app.childNamed(u'Status')
    company = app.childNamed(company_name)
    treeview = app.childNamed(u'gtkTreeView')
    companies = treeview.children
    status_field_index = companies.index(status)
    company_index = companies.index(company)
    company_status = companies[company_index + status_field_index]

    acceptbutton = app.button(u'Zaakceptuj')
    acceptbutton.click()

    assert company_status.text == 'ACCEPTED'
    company_status.click()

    rejectbutton = app.button(u'Odrzuć')
    rejectbutton.click()

    assert company_status.text == 'REJECTED'
    company_status.click()

    time.sleep(1)

    quitbutton = app.button(u'Zakończ')
    assert quitbutton is not None
    quitbutton.click()
  print("Test successful.")

if __name__ == "__main__":
  TestAddCompany()

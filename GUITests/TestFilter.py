#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
from dogtail import tree
from dogtail.utils import run
from dogtail.tree import SearchError
from TestUtils import *

import time

def TestEdit():
  with CEIDGClient() as client, CEIDGServer() as server:
    app = client.get_app();
    connectbutton = app.button(u'Połącz')
    connectbutton.click()

    wrong_company_name = u'Demland'
    company_name = u'Dmeland'
    nip  = '1111111111'
    nip1 = '7422162065'

    entry_namefilter = app.textentry(u'gtkEntryFilterName')
    assert entry_namefilter is not None
    entry_namefilter.click()
    entry_namefilter.typeText(wrong_company_name)
    
    wrongname = None
    try:
      wrongname = app.childNamed(wrong_company_name)
    except SearchError:
      pass
    assert wrongname is None

    time.sleep(1)

    clearbutton = app.button(u'Czyść filtr')
    assert clearbutton is not None
    clearbutton.click()

    entry_namefilter = app.textentry(u'gtkEntryFilterName')
    assert entry_namefilter is not None
    entry_namefilter.click()
    entry_namefilter.typeText(company_name)
    
    rightname = None
    rightname = app.childNamed(company_name)
    assert rightname is not None
    rightname.click()

    time.sleep(1)

    clearbutton = app.button(u'Czyść filtr')
    assert clearbutton is not None
    clearbutton.click()

    entry_nipfilter = app.textentry(u'gtkEntryFilterNIP')
    assert entry_nipfilter is not None
    entry_nipfilter.click()
    entry_nipfilter.typeText(nip)

    wrongnip = None
    try:
      wrongnip = app.childNamed(nip)
    except SearchError:
      pass
    assert wrongnip is None

    time.sleep(1)

    clearbutton = app.button(u'Czyść filtr')
    assert clearbutton is not None
    clearbutton.click()

    entry_nipfilter = app.textentry(u'gtkEntryFilterNIP')
    assert entry_nipfilter is not None
    entry_nipfilter.click()
    entry_nipfilter.typeText(nip1)

    rightnip = None
    rightnip = app.childNamed(nip1)
    assert rightnip is not None
    rightnip.click()

    time.sleep(1)

    clearbutton = app.button(u'Czyść filtr')
    assert clearbutton is not None
    clearbutton.click()

    entry_namefilter = app.textentry(u'gtkEntryFilterFirstName')
    assert entry_namefilter is not None
    entry_namefilter.click()
    entry_namefilter.typeText(u'Jakub')

    wrongname = None
    try:
      wrongname = app.childNamed(u'Jakub')
    except SearchError:
      pass
    assert wrongname is None

    time.sleep(1)

    clearbutton = app.button(u'Czyść filtr')
    assert clearbutton is not None
    clearbutton.click()

    entry_namefilter = app.textentry(u'gtkEntryFilterFirstName')
    assert entry_namefilter is not None
    entry_namefilter.click()
    entry_namefilter.typeText(u'Dakub')

    rightname = None
    rightname = app.childNamed(u'Dakub')
    assert rightname is not None
    rightname.click()

    time.sleep(1)

    clearbutton = app.button(u'Czyść filtr')
    assert clearbutton is not None
    clearbutton.click()

    quitbutton = app.button(u'Zakończ')
    assert quitbutton is not None
    quitbutton.click()
  print("Test successful.")

if __name__ == "__main__":
  TestEdit()

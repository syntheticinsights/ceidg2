#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
from dogtail import tree
from dogtail.utils import run
from dogtail.tree import SearchError
from TestUtils import *

import time

def TestEdit():
  with CEIDGClient() as client, CEIDGServer() as server:
    app = client.get_app();
    app.childNamed(u'Zaloguj').click()
    app.textentry(u'gtkEntryUsername').typeText(u'admin')
    app.childNamed(u'gtkEntryPassword').typeText(u'admin')
    app.button(u'Połącz').click()

    wrong_company_name = u'Dmeland'
    company_name = u'Demland'

    entry_namefilter = app.textentry(u'gtkEntryFilterName')
    assert entry_namefilter is not None
    entry_namefilter.click()
    entry_namefilter.typeText(wrong_company_name)

    wrongname = app.childNamed(wrong_company_name)
    assert wrongname is not None
    wrongname.click()

    editbutton = app.button(u'Edytuj dane')
    assert editbutton is not None
    editbutton.click()

    entry_name = app.textentry(u'gtkEntryCompanyName')
    assert entry_name is not None
    entry_name.click()
    entry_name.keyCombo("<Control>a")
    entry_name.typeText(company_name)

    entry_ownerfirst = app.textentry(u'gtkEntryFirstName')
    assert entry_ownerfirst is not None
    entry_ownerfirst.click()
    entry_ownerfirst.keyCombo("<Control>a")
    entry_ownerfirst.typeText(u'Jakub')

    entry_ownerlast = app.textentry(u'gtkEntryLastName')
    assert entry_ownerlast is not None
    entry_ownerlast.click()
    entry_ownerlast.keyCombo("<Control>a")
    entry_ownerlast.typeText(u'Debski')

    savebutton = app.button(u'Zapisz zmiany')
    assert savebutton is not None
    savebutton.click()

    feedback = app.childNamed(u'gtkLabelFeedback')
    assert feedback.text == 'Zmiany zapisane'

    browsetab = app.tab(u'Przeglądaj')
    assert browsetab is not None
    browsetab.click()

    entry_namefilter = app.textentry(u'gtkEntryFilterName')
    assert entry_namefilter is not None
    entry_namefilter.click()
    entry_namefilter.keyCombo("<Control>a")
    entry_namefilter.typeText(company_name)

    rightname = app.childNamed(company_name)
    assert wrongname is not None
    rightname.click()

    status = app.childNamed(u'Status')
    company = app.childNamed(company_name)
    treeview = app.childNamed(u'gtkTreeView')
    companies = treeview.children
    status_field_index = companies.index(status)
    company_index = companies.index(company)
    company_status = companies[company_index + status_field_index]

    assert company_status.text == 'WAITING'
    company_status.click()

    time.sleep(1)

    quitbutton = app.button(u'Zakończ')
    assert quitbutton is not None
    quitbutton.click()
  print("Test successful.")

if __name__ == "__main__":
  TestEdit()
